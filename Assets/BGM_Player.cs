using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGM_Player : MonoBehaviour
{
    public static BGM_Player Instance;
    public AudioClip normal, forEvent;
    private AudioSource audio;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        audio = GetComponent<AudioSource>();
        
    }

    public void PlayNormalBGM()
    {
        if (audio.clip == normal) return;
        audio.clip = normal;
        audio.Play();
    }

    public void PlayEventBGM()
    {
        if (audio.clip == forEvent) return;
        audio.clip = forEvent;
        audio.Play();
    }
}