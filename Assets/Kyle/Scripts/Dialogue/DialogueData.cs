using UnityEngine;

[CreateAssetMenu(fileName = "DialogueData", menuName = "ScriptableObject/DialogueData")]
public class DialogueData : ScriptableObject
{
    [System.Serializable]
    public struct DialogueInfo
    {
        public string id;
        public string speaker;
        public string content;
    }

    public DialogueInfo[] dialogueInfos;
}