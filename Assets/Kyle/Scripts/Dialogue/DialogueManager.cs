using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    public static DialogueManager Instance = null;
    public Image mainIcon;
    public GameObject goBtn;
    public GameObject goBtn2;

    [SerializeField] private float d_gap = 1.0f;

    private DialogueSystem dialogueSystem;

    private const string dictSpeakerPreStr = "s_";
    private const string dictContentPreStr = "c_";

    private int currentProgress = -1;
    private DialogueData currentData = null;

    private UnityAction FinishCB = null;
    private UnityAction FinishNoCB = null;

    [System.Serializable]
    private struct dialogueGroup
    {
        public string id;
        public DialogueData data;
    }

    [SerializeField] private dialogueGroup[] dialogueGroups;

    private void Awake()
    {
        if(Instance)
            Destroy(this);
        else
            Instance = this;
    }

    private void Start()
    {
        dialogueSystem = DialogueSystem.Instance;
    }

    private void OnDestroy()
    {
        Instance = null;
    }

    bool isNoSelect = false;
    public void PlayDialogue(string dialogueGroupID, UnityAction action , UnityAction action2 = null , bool _isNoSelect = false)
    {
        FinishCB = action;
        FinishNoCB = action2;
        isNoSelect = _isNoSelect;

        foreach (dialogueGroup item in dialogueGroups)
        {
            if(item.id.Equals(dialogueGroupID))
            {
                currentProgress = -1;
                currentData = item.data;
                for(int i = 0; i < item.data.dialogueInfos.Length; ++i)
                {
                    dialogueSystem.AddDialogue(dictSpeakerPreStr + item.data.dialogueInfos[i].id, item.data.dialogueInfos[i].content);
                    dialogueSystem.AddDialogue(dictContentPreStr + item.data.dialogueInfos[i].id, item.data.dialogueInfos[i].content);
                }
                ShowDialogue();
                return;
            }
        }
    }

    public void PlayLast() //直接播放最後一句
    {
        if (!isShowing) return;
        currentProgress = currentData.dialogueInfos.Length -2;
        ShowDialogue();
    }

    public bool isShowing = false;
    public void ShowDialogue()
    {
        goBtn.SetActive(false);
        goBtn2.SetActive(false);

        if (currentProgress < currentData.dialogueInfos.Length - 1)
        {
            
        }
        else
        {
            isShowing = false;
            if (FinishCB == null) //沒有call back直接關掉UI
            {
                dialogueSystem.ui.Close();
            }
            else if(isNoSelect)
            {
                FinishCB.Invoke();
                dialogueSystem.ui.Close();
            }
            else
            {
                goBtn.SetActive(true);
            }
            if (FinishNoCB != null) 
            {
                goBtn2.SetActive(true);
            }

            return;
        }

        isShowing = true;
        SoundHandler.Instance.PlaySFX(Sound.Click);

        ++currentProgress;
        if (currentData.dialogueInfos[currentProgress].speaker == "哥布林")
        {
            mainIcon.sprite = YuehMenu.instance.gobSprite;
        }
        else if (currentData.dialogueInfos[currentProgress].speaker == "玩家")
        {
            mainIcon.sprite = YuehMenu.instance.main_Image.sprite;
        }
        else if (currentData.dialogueInfos[currentProgress].speaker == "超派哥布林")
        {
            mainIcon.sprite = YuehMenu.instance.piGobSprite;
        }
        else if (currentData.dialogueInfos[currentProgress].speaker == "克洛蒂")
        {
            mainIcon.sprite = YuehMenu.instance.wupoSprite;
        }
        else if (currentData.dialogueInfos[currentProgress].speaker == "神秘商人")
        {
            mainIcon.sprite = YuehMenu.instance.TraderSprite;
        }
        else if (currentData.dialogueInfos[currentProgress].speaker == "史奈爾")
        {
            mainIcon.sprite = YuehMenu.instance.guNuSprite;
        }

        dialogueSystem.SetUITitle(currentData.dialogueInfos[currentProgress].speaker);
        dialogueSystem.SetUIContent(dictContentPreStr + currentData.dialogueInfos[currentProgress].id);
        dialogueSystem.OpenUI();
        
    }

    public void OnClickOK()
    {
        dialogueSystem.ui.CloseForce();
        if (FinishCB != null)
        {
            FinishCB.Invoke();
        }
    }

    public void OnClickNo()
    {
        dialogueSystem.ui.CloseForce();
        if(FinishNoCB != null)
        {
            FinishNoCB.Invoke();
        }
    }

    public void ChangeBtnText()
    {
        goBtn.transform.GetChild(0).GetComponent<Text>().text = "女巫";
        goBtn2.transform.GetChild(0).GetComponent<Text>().text = "蝸牛";
    }
}