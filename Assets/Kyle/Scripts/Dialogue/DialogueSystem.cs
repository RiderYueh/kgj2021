using System.Collections.Generic;
using UnityEngine;

public class DialogueSystem : MonoBehaviour
{
    public static DialogueSystem Instance;

    [SerializeField] public DialogueUI ui;

    private Dictionary<string, string> dialogueDict = new Dictionary<string, string>();

    private void Awake()
    {
        if(Instance)
            Destroy(this);
        else
            Instance = this;
    }

    private void OnDestroy()
    {
        Instance = null;
    }

    public void OpenUI()
    {
        ui.Open();
    }

    public void CloseUI()
    {
        ui.Close();
    }

    public void SetUITitle(string value)
    {
        ui.SetTitle(value);
    }

    public void SetUIContent(string key)
    {
        ui.SetContent(GetDialogue(key));
    }

    public string GetDialogue(string key)
    {
        return dialogueDict[key];
    }

    public void AddDialogue(string key, string text)
    {
        if(dialogueDict.ContainsKey(key))
        {
            return;
        }
        dialogueDict.Add(key, text);
    }

    public void RemoveDialogue(string key)
    {
        dialogueDict.Remove(key);
    }
}