using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class DialogueUI : MonoBehaviour
{
    [SerializeField] private GameObject uiGo;
    [SerializeField] private Transform uiTran;
    [SerializeField] private Text dialogueTitle;
    [SerializeField] private Text dialogueText;

    [System.Serializable]
    public struct AnimaPoint
    {
        public Vector3 size;
        public float duration;
        public Ease ease;
    }

    [SerializeField] private AnimaPoint[] openAnimaPoints;
    [SerializeField] private AnimaPoint[] closeAnimaPoints;

    void Start()
    {
        
    }

    public void SetTitle(string title)
    {
        dialogueTitle.text = title;
    }

    public void SetContent(string content)
    {
        dialogueText.text = content;
    }

    public void Open()
    {
        uiGo.SetActive(true);
        OpenAnima();
    }

    public void Close()
    {
        CloseAnima();
    }

    public void CloseForce()
    {
        uiGo.SetActive(false);
    }

    private void OpenAnima()
    {
        uiTran.transform.localScale = Vector3.zero;
        Sequence sequence = DOTween.Sequence();
        for(int i = 0; i < openAnimaPoints.Length; ++i)
        {
            sequence.Append(uiTran.DOScale(openAnimaPoints[i].size, openAnimaPoints[i].duration).SetEase(openAnimaPoints[i].ease));
        }
    }

    private void CloseAnima()
    {
        Sequence sequence = DOTween.Sequence();
        for(int i = 0; i < closeAnimaPoints.Length; ++i)
        {
            if(i == closeAnimaPoints.Length - 1)
            {
                sequence.Append(uiTran.DOScale(closeAnimaPoints[i].size, closeAnimaPoints[i].duration).SetEase(closeAnimaPoints[i].ease).OnComplete(() => { uiGo.SetActive(false); }));
            }
            else
            {
                sequence.Append(uiTran.DOScale(closeAnimaPoints[i].size, closeAnimaPoints[i].duration).SetEase(closeAnimaPoints[i].ease));
            }
        }
    }
}