﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PossibilityData))]
public class PossibilityDataEditor : Editor
{
    PossibilityData possibilityData;

    private void OnEnable()
    {
        possibilityData = (PossibilityData)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if(GUILayout.Button("Valid"))
        {
            int validWeight = 0;
            for(int i = 0; i < possibilityData.items.Length; ++i)
            {
                validWeight += possibilityData.items[i].weight;
            }
            if(validWeight != 10000)
            {
                Debug.LogError("權重設定錯誤，總和需要10000");
            }
            else
            {
                Debug.Log("機率設定無誤");
            }
        }
    }
}