﻿using System.Collections.Generic;
using UnityEngine;

public class LotteryCenter
{

    //prize的id, prize的機率 (?/10000)
    private readonly Dictionary<int, int> prizeData;

    //萬分之一，可做到0.01%
    private const int weightRange = 10000;

    //產生的機率列表
    private readonly int[] hitNumberSaver;

    //int value of startIndex to int value of randMax
    private readonly List<int> hitNumberRange;

    public LotteryCenter()
    {
        prizeData = new Dictionary<int, int>();
        hitNumberSaver = new int[weightRange];
        hitNumberRange = new List<int>();
        for(int i = 0; i < weightRange; ++i)
        {
            hitNumberRange.Add(i);
        }
    }

    public void AddPrize(int id, int prob)
    {
        prizeData.Add(id, prob);
    }

    public void RemovePrize(int id)
    {
        prizeData.Remove(id);
    }

    public void GenerateRandomList()
    {

        if(!CheckWeightTotal())
        {
            Debug.LogError("權重總和不等於10000!");
            return;
        }

        List<int> tempHitNumberRange = hitNumberRange.GetRange(0, hitNumberRange.Count);

        foreach(var item in prizeData)
        {
            int weight = item.Value;
            int progress = 0;
            while(progress < weight)
            {
                int weightLocation = Random.Range(0, weightRange);
                if(tempHitNumberRange.Contains(weightLocation))
                {
                    hitNumberSaver[weightLocation] = item.Key;
                    tempHitNumberRange.Remove(weightLocation);
                    ++progress;
                }
            }
        }
    }

    private bool CheckWeightTotal()
    {
        int total = 0;
        foreach(var item in prizeData)
        {
            total += item.Value;
        }

        return total == weightRange;
    }

    public int GetPrize()
    {
        return hitNumberSaver[Random.Range(0, hitNumberSaver.Length)];
    }
}