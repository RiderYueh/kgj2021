﻿using UnityEngine;

[CreateAssetMenu(fileName = "PossibilityData", menuName = "ScriptableObject/PossibilityData")]
[System.Serializable]
public class PossibilityData : ScriptableObject
{
    [System.Serializable]
    public struct Item
    {
        public string name;
        public int id;
        public int weight;
    }

    public Item[] items;
}