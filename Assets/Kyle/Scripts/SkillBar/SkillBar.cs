using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillBar : MonoBehaviour
{
    public static SkillBar Instance = null;

    [SerializeField] private GameObject goSkillDes;
    [SerializeField] private Text textSkillDes;
    [SerializeField] private Button btnAllOk;
    [SerializeField] private GameObject labelObj;
    [SerializeField] private Transform labelGrid;
    [SerializeField] private Image imgHighlight;
    [SerializeField] private RectTransform barSize;

    [SerializeField] private int leastTimes = 10;

    [SerializeField] private float changingGapTime = 0.2f;

    private int runNo = 0;
    private int runTimes = 0;
    private int finalKeyNo = 0;
    public int CurrentSkill
    {
        get
        {
            return finalKeyNo;
        }
    }

    private List<Text> skillLabelList = new List<Text>();
    private List<int> lockedSkillList = new List<int>();

    private void Awake()
    {
        if (Instance)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    public void ChangeAllOkBtnColor(bool isOpen)
    {
        if(isOpen) btnAllOk.GetComponent<Image>().color = Color.white;
        else btnAllOk.GetComponent<Image>().color = Color.gray;
    }

    private void Start()
    {
        btnAllOk.onClick.AddListener(OnSkillClicked);
        ChangeAllOkBtnColor(false);
    }
    public delegate void MyKeyDelegate(int _index);
    MyKeyDelegate EndFuc;
    public void SetEndFunction(MyKeyDelegate _cb)
    {
        EndFuc = _cb;
    }
    public void InitLabels(int count = 5)
    {
        skillLabelList.Clear();
        foreach(Transform child in labelGrid)
        {
            Destroy(child.gameObject);
        }
        for (int i = 0; i < count; i++)
        {
            GameObject go = Instantiate(labelObj, labelGrid);
            go.SetActive(true);
            go.GetComponent<Image>().color = Color.gray;
            skillLabelList.Add(go.GetComponentInChildren<Text>());
        }
    }

    /*public void OpenSkillDescription(bool isOpen)
    {
        for (int i = 0; i < skillLabelList.Count; i++)
        {
            skillLabelList[i].raycastTarget = isOpen;
        }
    }*/

    public void InitSkills(string[] skills,int[] skillIDs)
    {
        if (skills != null)
        {
            for (int i = 0; i < skillLabelList.Count; i++)
            {
                SetButtonText(i, skills[i] , skillIDs[i]);
            }
        }
        else
        {
            string skillStr = "skill";
            for (int i = 0; i < skillLabelList.Count; i++)
            {
                SetButtonText(i, skillStr + (i + 1) , 1);
            }
        }
    }

    public void OnClickGo(GameObject go)
    {
        go.SetActive(false);
        BGM_Player.Instance.PlayEventBGM();
        BattleManager.instance.Run(BattleManager.EnumRunTarget.Camp);
    }

    private void SetButtonText(int no, string btnText , int skillID)
    {
        skillLabelList[no].text = btnText;
        skillLabelList[no].gameObject.name = skillID.ToString();
    }

    public void OnSkillClicked()
    {
        SoundHandler.Instance.PlaySFX(Sound.Button);

        if (BattleManager.instance.m_PlayerState != BattleManager.PlayerState.Fight)
        {
            YuehPlayerData.instance.ShowDialog("沒有戰鬥");
            return;
        }
        else
        {
            if(BattleManager.instance.IsCanUseSkill())
            {
                BattleManager.instance.isClickSkill = true;
                RandomKey();
                imgHighlight.enabled = true;
                runNo = -1;
                runTimes = 0;
                StartCoroutine(RunLight());
            }
        }
        
    }

    private WaitForFixedUpdate waitTime;
    private IEnumerator RunLight()
    {
        yield return waitTime;
        ++runNo;
        if (runNo < skillLabelList.Count)
        {
            imgHighlight.transform.position = skillLabelList[runNo].transform.position;
        }
        else
        {
            runNo = 0;
            imgHighlight.transform.position = skillLabelList[runNo].transform.position;
        }

        ++runTimes;
        if (runTimes < leastTimes)
        {
            StartCoroutine(RunLight());
        }
        else
        {
            if (runNo != finalKeyNo)
            {
                StartCoroutine(RunLight());
            }
            else
            {

                EndFuc(CurrentSkill);
            }
        }
    }

    private void RandomKey()
    {
        List<int> list_ranKey = new List<int>();
        int startNum = YuehPlayerData.instance.lockSkillCount;

        for (int i = startNum; i < BattleManager.instance.Actions.Count; i++) //喜歡在後面 讓前面抽到機會變3倍
        {
            list_ranKey.Add(i);
            if (BattleManager.instance.Actions[i].SkillID == 11)
            {
                for (int j = startNum; j < i; j++)
                {
                    list_ranKey.Add(j);
                    list_ranKey.Add(j);
                }
            }
        }

        
        finalKeyNo = Random.Range(0, list_ranKey.Count);
        finalKeyNo = list_ranKey[finalKeyNo];
        int max = 1000;

        if (BattleManager.instance.isSkill10)
        {
            bool isHaveAttack = BattleManager.instance.IsHaveAttackAction();
            while (BattleManager.instance.Actions[finalKeyNo].MyAction != ActionType.Attack && isHaveAttack && max > 0)
            {
                finalKeyNo = Random.Range(0, list_ranKey.Count);
                finalKeyNo = list_ranKey[finalKeyNo];
                max--;
            }
        }

        /*if (lockedSkillList.Count > 0)
        {
            while (lockedSkillList.Contains(finalKeyNo) && max > 0)
            {
                finalKeyNo = Random.Range(0, skillLabelList.Count);
                max--;
            }
        }*/
    }

    public void OnPointEnterShowSkill(GameObject go)
    {
        goShowSkill = go.transform.parent.gameObject;
        PlayerAction pa = BattleManager.instance.GetSkill(System.Convert.ToInt32(go.name));
        ShowSkillDescript(pa.Description);
        
    }

    public void OnPointEnterExit()
    {
        goSkillDes.SetActive(false);
        goShowSkill.GetComponent<Image>().color = Color.gray;
        goShowSkill = null;
    }

    public GameObject goShowSkill;
    public void ShowSkillDescript(string str)
    {
        goShowSkill.GetComponent<Image>().color = new Color(0.6f,0.6f,1);
        goSkillDes.SetActive(true);
        textSkillDes.text = str;
    }


}