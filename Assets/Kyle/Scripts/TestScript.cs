using UnityEngine;
using UnityEngine.UI;

public class TestScript : MonoBehaviour
{
    public DialogueUI ui;
    public Button open;
    public Button close;

    void Start()
    {
        open.onClick.AddListener(() => { ui.Open(); });
        close.onClick.AddListener(() => { ui.Close(); });
    }
}