using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BoxSceneManager : MonoBehaviour
{
    public static System.Action OnLevelPass;
    private int inBoxBlocks = 0;

    public List<GameObject> blocks = new List<GameObject>();
    public GameObject wowImage;

    

    private void OnEnable()
    {
        SceneManager.sceneLoaded += PlayIntro;
    }

    private void PlayIntro(Scene scene, LoadSceneMode mode)
    {
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            foreach (GameObject g in blocks)
            {
                Instantiate(g, new Vector3(0, 5, 0), Quaternion.identity);
            }
        }
#if UNITY_EDITOR
        if(Input.GetKeyDown("s"))
        {
            SceneCutter.Instance.LoadScene("Battle", 2);
        }
#endif
    }

    private void OnTriggerEnter(Collider other)
    {
        inBoxBlocks++;
        SoundHandler.Instance.PlaySFX(Sound.Lottery);
        if (inBoxBlocks >= 4)
        {
            inBoxBlocks = 0;
            OnLevelPass?.Invoke();
            SceneCutter.Instance.LoadScene("Battle", 2);
        }
        else
        {
            //StartCoroutine(PlayEffect());
        }
    }

    private IEnumerator PlayEffect()
    {
        wowImage.SetActive(true);
        wowImage.GetComponent<Animator>().Play("WOW");
        yield return new WaitForSeconds(1);
        SoundHandler.Instance.PlaySFX(Sound.wow);
        yield return new WaitForSeconds(3);
        wowImage.SetActive(false);
    }
}