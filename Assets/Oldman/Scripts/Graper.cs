using UnityEngine;

public class Graper : MonoBehaviour
{
    static public Graper instance;
    public AudioSource m_AudioSource;
    public LayerMask blockMask;
    public float grapHeight;
    public float moveSpeed = 50f;
    private Transform target;
    private bool Isgrapping = false;

    private void Awake()
    {
        if (instance == null) instance = this;
    }

    private void Start()
    {
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            GrapUp();
        }
        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            Release();
        }

        if (Isgrapping)
        {
            Move();
        }
    }

    private void GrapUp()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        //Ray ray = new Ray(Input.mousePosition, Camera.main.transform.forward);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, blockMask))
        {
            target = hit.transform;
            target.transform.position = new Vector3(target.transform.position.x, grapHeight, target.transform.position.z);
            GrapEffectOn();
            Isgrapping = true;
        }
    }

    private void Release()
    {
        GrapEffectOff();
        Isgrapping = false;
    }

    private void Move()
    {
        Vector3 input = new Vector3(Input.GetAxisRaw("Mouse X"), 0, Input.GetAxisRaw("Mouse Y"));
        input = Vector3.ClampMagnitude(input, 0.1f);
        target.transform.position += input * Time.deltaTime * moveSpeed;
    }

    private void GrapEffectOn()
    {
        if (target == null) return;
        target.GetComponent<Rigidbody>().useGravity = false;
        target.GetComponent<Collider>().enabled = false;
        Cursor.visible = false;
        //edge glow
    }

    private void GrapEffectOff()
    {
        if (target == null) return;
        target.GetComponent<Rigidbody>().useGravity = true;
        target.GetComponent<Rigidbody>().velocity = Vector3.zero;
        target.GetComponent<Collider>().enabled = true;

        Cursor.visible = true;
        //edge glow
    }
}