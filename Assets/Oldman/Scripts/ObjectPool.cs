﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    private Queue<GameObject> Pool = new Queue<GameObject>();
    private GameObject Prefab;

    public static ObjectPool CreatPool(string poolName, GameObject prefab, int initAmount)
    {
        GameObject g = new GameObject(poolName);
        var pool = g.AddComponent<ObjectPool>();
        pool.InitPool(prefab, initAmount);
        return pool;
    }

    public void InitPool(GameObject prefab, int initAmount)
    {
        Prefab = prefab;
        for (int i = 0; i < initAmount; i++)
        {
            GameObject newPrefab = Instantiate(Prefab, transform);
            Pool.Enqueue(newPrefab);
            newPrefab.SetActive(false);
        }
    }

    public static ObjectPool CreatPool(string poolName, GameObject prefab, Transform parent, int initAmount)
    {
        GameObject g = new GameObject(poolName);
        var pool = g.AddComponent<ObjectPool>();
        pool.InitPool(prefab, parent, initAmount);
        return pool;
    }

    public void InitPool(GameObject prefab, Transform parent, int initAmount)
    {
        Prefab = prefab;
        for (int i = 0; i < initAmount; i++)
        {
            GameObject newPrefab = Instantiate(Prefab, parent);
            Pool.Enqueue(newPrefab);
            newPrefab.SetActive(false);
        }
    }

    public GameObject Use()
    {
        if (Pool.Count > 0)
        {
            GameObject prefab = Pool.Dequeue();
            prefab.SetActive(true);
            return prefab;
        }
        else
        {
            GameObject newPrefab = Instantiate(Prefab, transform);
            newPrefab.SetActive(false);
            newPrefab.SetActive(true);
            return newPrefab;
        }
    }

    public GameObject Use(Vector3 position, Quaternion rotation)
    {
        GameObject newPrefab = Instantiate(Prefab, position, rotation, transform);

        return newPrefab;
    }

    public GameObject Use(Vector3 position, Quaternion rotation, float afterSec)
    {
        if (Pool.Count > 0)
        {
            GameObject prefab = Pool.Dequeue();
            prefab.transform.position = position;
            prefab.transform.rotation = rotation;
            prefab.SetActive(true);
            Recycle(prefab, afterSec);
            return prefab;
        }
        else
        {
            GameObject newPrefab = Instantiate(Prefab, position, rotation, transform);
            newPrefab.SetActive(false);
            newPrefab.SetActive(true);
            Recycle(newPrefab, afterSec);
            return newPrefab;
        }
    }

    public void Recycle(GameObject prefab)
    {
        if (prefab == null) return;

        prefab.gameObject.SetActive(false);
        Pool.Enqueue(prefab);
    }

    public void Recycle(GameObject prefab, float afterSec)
    {
        StartCoroutine(DoRecycleAfterSec(prefab, afterSec));
    }

    private IEnumerator DoRecycleAfterSec(GameObject prefab, float sec)
    {
        yield return new WaitForSeconds(sec);
        Recycle(prefab);
    }
}