﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneCutter : MonoBehaviour
{
    public static SceneCutter Instance;
    public Image BlackPanel;
    public Text text;

    private bool IsLoading;

    private void Awake()
    {
        Instance = this;
    }

    public void FadeOutIn(float outSec, float inSec, float midSec)
    {
        StartCoroutine(DoFadeOutIn(outSec, inSec, midSec));
    }

    private IEnumerator DoFadeOutIn(float outSec, float inSec, float midSec)
    {
        StartCoroutine(DoFadeOut(outSec));
        yield return new WaitForSeconds(midSec);
        StartCoroutine(DoFadeIn(inSec));
    }

    public void FadeIn(Scene current, Scene next)
    {
        StartCoroutine(DoFadeIn(1f));
    }

    private IEnumerator DoFadeIn(float sec)
    {
        float alpha = 1;
        while (alpha > 0)
        {
            BlackPanel.color = new Color(BlackPanel.color.r, BlackPanel.color.g, BlackPanel.color.b, alpha);
            alpha -= Time.deltaTime / sec;
            yield return null;
        }
    }

    public void FadeOut(float sec)
    {
        StartCoroutine(DoFadeOut(sec));
    }

    private IEnumerator DoFadeOut(float sec)
    {
        float alpha = 0;
        while (alpha < 1)
        {
            BlackPanel.color = new Color(BlackPanel.color.r, BlackPanel.color.g, BlackPanel.color.b, alpha);
            if (SceneManager.GetActiveScene().name == "Battle")
            {
                text.color = new Color(BlackPanel.color.r, BlackPanel.color.g, BlackPanel.color.b, alpha);
            }
            alpha += Time.deltaTime / sec;
            yield return null;
        }
    }

    public void LoadScene(string sceneName)
    {
        LoadScene(sceneName, 2f);
    }

    public void LoadScene(string sceneName, float fadeSec)
    {
        StartCoroutine(DoLoadScene(sceneName, fadeSec));
    }

    private IEnumerator DoLoadScene(string sceneName, float fadeSec)
    {
        if (IsLoading) yield break;
        IsLoading = true;
        FadeOut(fadeSec);
        yield return new WaitForSeconds(fadeSec);
        SceneManager.LoadScene(sceneName);
    }
}