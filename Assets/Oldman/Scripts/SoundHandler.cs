﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public enum Sound
{
    smallExplosion,
    bigExplosion,
    Attack,
    FastAttack,
    HeavyAttack,
    Charge,
    Hurt,
    Block,
    Sleep,
    Die,
    Drink,
    Lottery,
    Button,
    C8763,
    wow,
    Click,
    Laugh,
    EnemyBreak,
}

public class SoundHandler : MonoBehaviour
{
    private static SoundHandler _instance;

    public static SoundHandler Instance
    {
        get
        {
            if (_instance == null)
            {
                print(Application.dataPath);
                GameObject g = Instantiate(Resources.Load("SoundHandler") as GameObject);
                DontDestroyOnLoad(g);
                _instance = g.GetComponent<SoundHandler>();
            }

            return _instance;
        }
    }

    public static Action OnBGMStart;
    public static Action OnBGMEnd;

    private ObjectPool Pool;
    private AudioSource Audio;

    private string CurrentBGM_Name;

    public AudioMixerGroup BGM_Mixer;
    public List<BGM_Data> BGMList = new List<BGM_Data>();
    public List<SFXData> SFXList = new List<SFXData>();

    private void Awake()
    {
        Audio = GetComponent<AudioSource>();
    }

    private void InitializePool()
    {
        GameObject audioObject = new GameObject("audioObject");
        DontDestroyOnLoad(audioObject);
        AudioSource source = audioObject.AddComponent<AudioSource>();
        source.loop = false;
        Pool = ObjectPool.CreatPool("AudioObjectPool", audioObject, transform, 5);
    }

    private void OnEnable()
    {
        InitializePool();
    }

    public void PlaySFX(Sound name)
    {
        SFXData data = GetSFX_Data(name);
        if(YuehPlayerData.instance != null)
        {
            YuehPlayerData.instance.m_AudioSource.PlayOneShot(data.Clip);
        }
        else
        {
            Graper.instance.m_AudioSource.PlayOneShot(data.Clip);
        }
        
    }

    public void PlaySFX(Sound name, Vector3 position)
    {
        SFXData data = GetSFX_Data(name);

        GameObject audioObject = Pool.Use(position, Quaternion.identity);

        AudioSource source = audioObject.GetComponent<AudioSource>();

        source.clip = data.Clip;
        source.volume = data.Volume;
        source.pitch = data.Pitch;
        source.outputAudioMixerGroup = data.Mixer;
        source.Play();

        Timer(data.Clip.length, () => Pool.Recycle(audioObject));
    }

    private SFXData GetSFX_Data(Sound name)
    {
        for (int i = 0; i < SFXList.Count; i++)
        {
            SFXData data = SFXList[i];
            if (data == null)
            {
                continue;
            }
            if (data.Name == name)
            {
                return data;
            }
        }

        Debug.Log($"SFX_Data：{name} not found");
        return null;
    }

    public void PlayBGM(string BGMname)
    {
        BGM_Data data = GetBGM_Data(BGMname);
        Audio.clip = data.Clip;
        StartCoroutine(FadeTo(data.MinMaxVolume.x, data.MinMaxVolume.y, data.FadeInSec));
        Audio.outputAudioMixerGroup = BGM_Mixer;
        Audio.Play();
    }

    private BGM_Data GetBGM_Data(string name)
    {
        for (int i = 0; i < BGMList.Count; i++)
        {
            BGM_Data data = BGMList[i];
            if (data == null)
            {
                continue;
            }
            if (data.Name == name)
            {
                return data;
            }
        }

        Debug.Log($"BGMList：{name} not found");
        return null;
    }

    private IEnumerator FadeTo(float initVolume, float EndVolume, float sec)
    {
        Audio.volume = initVolume;

        float ShiftPerSec = (EndVolume - initVolume) / sec;

        if (initVolume < EndVolume)
        {
            while (Audio.volume < EndVolume)
            {
                Audio.volume += ShiftPerSec * Time.deltaTime;
                yield return null;
            }
        }
        else
        {
            while (Audio.volume > EndVolume)
            {
                Audio.volume += ShiftPerSec * Time.deltaTime;
                yield return null;
            }
            Audio.Stop();
        }
    }

    public void PlayCurrentBGM()
    {
        PlayBGM(CurrentBGM_Name);
    }

    public void StopCurrentBGM()
    {
        BGM_Data data = GetBGM_Data(CurrentBGM_Name);
        StartCoroutine(FadeTo(data.MinMaxVolume.x, data.MinMaxVolume.y, data.FadeOutSec));
    }

    private void Timer(float time, Action EndOperation)
    {
        StartCoroutine(TimerCoroutine(time, EndOperation));
    }

    private IEnumerator TimerCoroutine(float time, Action EndOperation)
    {
        yield return new WaitForSeconds(time);
        EndOperation?.Invoke();
    }
}

[Serializable]
public class BGM_Data
{
    public string Name;
    public AudioClip Clip;

    [MinMaxSlider]
    public Vector2 MinMaxVolume;

    public int FadeInSec;
    public int FadeOutSec;
}