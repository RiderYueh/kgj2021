﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Effect
{
    smallExplosion,
    bigExplosion,
    heal,
    hit,
    charge,
    wow,
    protect,
    protect2,
    Water,
    HitEnemy,
    HitEnemy2,
    HitEnemyBreak,
}

public class VfxHandler : MonoBehaviour
{
    private static VfxHandler _instance;

    public static VfxHandler Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = Instantiate(Resources.Load("VfxHandler") as GameObject).GetComponent<VfxHandler>();
            }
            return _instance;
        }
    }

    public List<VFX> vfxList = new List<VFX>();

    private void OnEnable()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    private void Start()
    {
    }



    public VFX FindVFX(Effect name)
    {
        for (int i = 0; i < vfxList.Count; i++)
        {
            if (name == vfxList[i].name)
            {
                return vfxList[i];
            }
        }
        print($"can't find vfx ：{name}");
        return null;
    }

    public GameObject PlayVFX(Effect vfxName, Vector3 pos, Quaternion rotation, float destoryTime = 3)
    {
        VFX v = FindVFX(vfxName);

        GameObject newPrefab = Instantiate(v.prefab, pos, rotation, transform) as GameObject;

        Destroy(newPrefab, destoryTime);

        return newPrefab;
    }

}

[Serializable]
public class VFX
{
    public Effect name;
    public GameObject prefab;

}