using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Scoz.Func;
using System.Linq;

public partial class BattleManager : MonoBehaviour
{
    public PlayerAction AddESkill(int _value)
    {
        PlayerAction pa = new PlayerAction();
        pa.SkillID = _value;


        if (_value == 1) // 哥布林攻擊
        {
            pa.MyAction = ActionType.Attack;
            
            pa.Values = new List<float>() { 8, 1, 0, 1 };//攻擊力,攻擊次數,續力時間,CD
        }
        else if (_value == 2)// 哥布林防禦
        {
            pa.MyAction = ActionType.Block;
            pa.Values = new List<float>() { 0 , 2};//CD 持續時間
        }
        else if (_value == 3)// 哥布林蓄力打 2 *12
        {
            pa.MyAction = ActionType.Attack;
            pa.Values = new List<float>() { 12, 2, 3, 1 ,1000};//攻擊力,攻擊次數,續力時間,CD 受到多少傷害停止動作
        }
        else if (_value == 4)// 派哥布林打 1 *12
        {
            pa.MyAction = ActionType.Attack;
            pa.Values = new List<float>() { 6, 1, 0, 1 };//攻擊力,攻擊次數,續力時間,CD
        }
        else if (_value == 5)// 派哥布林打 1 *16
        {
            pa.MyAction = ActionType.Attack;
            pa.Values = new List<float>() { 12, 1, 0, 1 };//攻擊力,攻擊次數,續力時間,CD
        }
        else if (_value == 6)// 派哥布林蓄力5 打 70
        {
            pa.MyAction = ActionType.Attack;
            pa.Values = new List<float>() { 70, 1, 8, 1, 48 };//攻擊力,攻擊次數,續力時間,CD 受到多少傷害停止動作
        }
        else if (_value == 7)// 派哥布林蓄力4 打 4*6
        {
            pa.MyAction = ActionType.Attack;
            pa.Values = new List<float>() { 6, 4, 4, 1, 1000 };//攻擊力,攻擊次數,續力時間,CD 受到多少傷害停止動作
        }
        else if (_value == 8)// 蝸牛打1*10
        {
            pa.MyAction = ActionType.Attack;
            pa.Values = new List<float>() { 10, 1, 0, 1};//攻擊力,攻擊次數,續力時間,CD 
        }
        else if (_value == 9)// 蝸牛發呆5秒
        {
            pa.MyAction = ActionType.EnemySpecial;
            pa.Values = new List<float>() { 5 };//持續 
        }
        else if (_value == 10)// 蝸牛防禦
        {
            pa.MyAction = ActionType.Block;
            pa.Values = new List<float>() { 0, 5 };//CD 持續時間
        }
        else if (_value == 11)// 蝸牛發呆3秒
        {
            pa.MyAction = ActionType.EnemySpecial;
            pa.Values = new List<float>() { 3};//持續 cd
        }
        else if (_value == 12)// 蝸牛防禦2秒
        {
            pa.MyAction = ActionType.Block;
            pa.Values = new List<float>() { 0, 2 };//CD 持續時間
        }
        else if (_value == 13)// 蝸牛旋轉防禦3秒
        {
            pa.MyAction = ActionType.Block;
            pa.Values = new List<float>() { 0, 3 };//CD 持續時間
        }
        else if (_value == 14)// 蝸牛打3*10
        {
            pa.MyAction = ActionType.Attack;
            pa.Values = new List<float>() { 10, 3, 0, 1 };//攻擊力,攻擊次數,續力時間,CD 
        }
        else if (_value == 15)//女巫丟藥水
        {
            pa.MyAction = ActionType.EnemySpecial;
            pa.Values = new List<float>() {1};//CD 
        }
        else if (_value == 16)// 女巫蓄力5秒 (期間承受150傷害 中斷方案B)
        {
            pa.MyAction = ActionType.Attack;
            pa.Values = new List<float>() { 888, 1, 5, 1, 150 };//攻擊力,攻擊次數,續力時間,CD 受到多少傷害停止動作 888特殊攻擊
        }
        else if (_value == 17)// 女巫打20
        {
            pa.MyAction = ActionType.Attack;
            pa.Values = new List<float>() { 20, 1, 0, 1 };//攻擊力,攻擊次數,續力時間,CD 
        }
        else if (_value == 18)// 蝸牛次次
        {
            pa.MyAction = ActionType.Block;
            pa.Values = new List<float>() { 0, 6 };//CD 持續時間
        }
        else if (_value == 19)// 發呆19秒
        {
            pa.MyAction = ActionType.EnemySpecial;
            pa.Values = new List<float>() { 19 };//持續 
        }

        return pa;
    }
    List<List<int>> EnemyAI = new List<List<int>>();
    public int EnemyID = 0;
    public void InitEnemySkill(int _id)
    {
        EnemyID = _id; 
        Debug.Log("InitEnemySkill " + _id.ToString());
        CurAIActionIndex = 0;
        CurAIIndex = 0;
        EnemyAI.Clear();
        switch (_id) // 1哥不林 0鍋牛 2超派 3女巫
        {
            case 0: //0鍋牛
                EnemyAI.Add(new List<int>() { 8, 9, 8 }); //20%

                EnemyAI.Add(new List<int>() { 10,9,13,14 });//40%
                EnemyAI.Add(new List<int>() { 10, 9, 13, 14 });//40%

                EnemyAI.Add(new List<int>() { 10, 18, 11,});//40%
                EnemyAI.Add(new List<int>() { 10, 18, 11, });//40%

                EActions.Add(AddESkill(8));
                EActions.Add(AddESkill(9));
                EActions.Add(AddESkill(10));
                EActions.Add(AddESkill(11));
                EActions.Add(AddESkill(13));
                EActions.Add(AddESkill(14));
                EActions.Add(AddESkill(18));

                break;
            case 1://1哥不林
                EnemyAI.Add(new List<int>() { 1, 2, 1 }); //60%
                EnemyAI.Add(new List<int>() { 1, 2, 1 });
                EnemyAI.Add(new List<int>() { 1, 2, 1 });
                EnemyAI.Add(new List<int>() { 3 }); //40%
                EnemyAI.Add(new List<int>() { 3 });
                EActions.Add(AddESkill(1));
                EActions.Add(AddESkill(2));
                EActions.Add(AddESkill(3));

                break;
            case 2: //超派
                EnemyAI.Add(new List<int>() { 4,5 }); //50%
                EnemyAI.Add(new List<int>() { 4, 5 }); //50%
                EnemyAI.Add(new List<int>() { 4, 5 }); //50%
                EnemyAI.Add(new List<int>() { 4, 5 }); //50%
                EnemyAI.Add(new List<int>() { 4, 5 }); //50%
                EnemyAI.Add(new List<int>() { 6,9 }); //20%
                EnemyAI.Add(new List<int>() { 6,9 }); //20%

                EnemyAI.Add(new List<int>() { 5,7 }); //20%
                EnemyAI.Add(new List<int>() { 5, 7 }); //20%
                EnemyAI.Add(new List<int>() { 5, 7 }); //20%

                EActions.Add(AddESkill(4));
                EActions.Add(AddESkill(5));
                EActions.Add(AddESkill(6));
                EActions.Add(AddESkill(7));
                EActions.Add(AddESkill(9));

                break;

            case 3: //女巫
                EnemyAI.Add(new List<int>() { 15 }); //60%
                EnemyAI.Add(new List<int>() { 15 }); //60%
                EnemyAI.Add(new List<int>() { 15 }); //60%
                EnemyAI.Add(new List<int>() { 16 }); //20%
                EnemyAI.Add(new List<int>() { 17 }); //20%


                EActions.Add(AddESkill(15));
                EActions.Add(AddESkill(15));
                EActions.Add(AddESkill(16));
                EActions.Add(AddESkill(17));

                break;
        }

    }

    public List<PlayerAction> EActions = new List<PlayerAction>();
    PlayerAction CurEnemyAction = null;
    public bool isSavePowerEnemy = false;
    public int damageBreakAction = 0;
    public bool isReDamage = false;
    public void EnemyTakeAction(int _index)
    {
        isSavePowerEnemy = false;
        foreach(PlayerAction pa in EActions)
        {
            if(pa.SkillID == _index)
            {
                CurEnemyAction = pa.GetCopy();
                break;
            }
        }
        
        switch (CurEnemyAction.MyAction)
        {
            case ActionType.Attack:
                if (CurEnemyAction.Values[2] > 0) //蓄力
                {
                    damageBreakAction = 0;
                    isSavePowerEnemy = true;
                    EnemyAttackTimer = new MyTimer(CurEnemyAction.Values[2], StartEnemyAttack, true, false);
                    ERole.MyAni.SetTrigger("Charging");
                }
                else
                {
                    StartEnemyAttack();
                }
                break;
            case ActionType.Block:
                if(_index == 13)
                {
                    ERole.MyAni.SetTrigger("Rotate");
                    Debug.Log("旋轉蝸牛");
                }
                if (_index == 18)
                {
                    Debug.Log("反傷蝸牛");
                    isReDamage = true;
                }
                ERole.Defend(isReDamage);
                EnemyAttackTimer = new MyTimer(CurEnemyAction.Values[1], AfterDefend, true, false);
                break;
            case ActionType.EnemySpecial:
                if(_index == 9 || _index ==11 || _index == 19) //發呆 持續 cd
                {
                    EnemyAttackTimer = new MyTimer(CurEnemyAction.Values[0], EnemyAction, true, false);

                    if(_index == 11)
                    {
                        SoundHandler.Instance.PlaySFX(Sound.Laugh);
                        ERole.MyAni.SetTrigger("Charging");
                        EnemyAttackTimer = new MyTimer(CurEnemyAction.Values[0], ()=> { ERole.MyAni.SetTrigger("Idle"); EnemyAction(); }, true, false);
                    }
                }
                if (_index == 15) //丟藥水
                {
                    EnemyAttackTimer = new MyTimer(CurEnemyAction.Values[0], EnemyAction15, true, false);
                    PlaySFX(Effect.Water, true);
                }

                break;
        }
    }

    void EnemyAction15()
    {
        ERole.MyAni.SetTrigger("Attack");
        BattleManager.instance.GetRandomSkill();
        EnemyAction();
    }

    void AfterDefend()
    {
        isReDamage = false;
        ERole.EndDefend();
        EnemyAction();
    }

    void StartEnemyAttack()
    {
        CurEnemyAction.Values[1]--;
        ERole.MyAni.speed = 1;

        if(CurEnemyAction.Values[0] == 888)
        {
            CurEnemyAction.Values[0] = Actions.Count * 5;
        }

        if ((int)CurEnemyAction.Values[1] >= 0)
        {
            ERole.MyAni.speed = CurEnemyAction.Values[1] + 1;
            EAttack((int)CurEnemyAction.Values[0]);
            EnemyAttackTimer = new MyTimer(0.1f, StartEnemyAttack, true, false);
        }
        else
        {
            EnemyAttackTimer = null;
            EnemyAction();
        }


    }
}