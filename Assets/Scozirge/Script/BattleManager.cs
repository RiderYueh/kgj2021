using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Scoz.Func;
using System.Linq;
using System.Collections;
using DG.Tweening;

public enum EActionType
{
    Attack,
    DoNothing,
}

public partial class BattleManager : MonoBehaviour
{
    public SpriteRenderer PRoleHead;
    public PlayerRole PRole = null;
    public EnemyRole ERole = null;
    public Image PHPImg = null;
    public GameObject PHPDialog = null;
    public Image EHPImg = null;
    public Image CDImg = null;
    public static BattleManager instance = null;

    public List<PlayerAction> Actions = new List<PlayerAction>();
    public SkillPanel SkillPanel;

    public GameObject EnemyHPUI = null;

    public Animator BattleAni = null;


    MyTimer EnemyAttackTimer = null;
    MyTimer EnemyCDTimer = null;


    MyTimer RunToEventTimer = null;
    MyTimer PlayerAttackTimer = null;
    public MyTimer PlayerCDTimer = null;
    MyTimer PlayerSleepTimer = null;
    MyTimer PlayerSkill8Timer = null;
    MyTimer PlayerSkill9Timer = null;
    MyTimer PlayerSkill14Timer = null;
    int powerOfSkill8 = 1;


    float CDBase = 1;//CD權重

    public PlayerState m_PlayerState;

    public int PlayerActionCount; //玩家行動次數

    public enum PlayerState
    {
        None,
        Run,
        Fight,
    }

    private void Awake()
    {
        instance = this;
    }

    public int stageNow = 0;
    private void Start()
    {
        PRole.Reborn();
        PlayerActionCount = 0;
        ERole.InitRole();
        InitPlayerSkill();
        stageNow = 0;

        m_PlayerState = PlayerState.None;
    }
    bool isGameEnd = false;
    void StartRandEvent(EnumRunTarget ert)
    {
        YuehLoopBG.instance.DoMove(false);
        BGM_Player.Instance.PlayEventBGM();

        if (isGameEnd)
        {
            EndGame();
            return;
        }

        if(stageNow >= 10 && ert == EnumRunTarget.Event)
        {
            ert = EnumRunTarget.Camp;
        }

        if(ert == EnumRunTarget.Camp)
        {
            YuehBreakOrNot.instance.GetNext();
        }
        else if (ert == EnumRunTarget.Event)
        {
            YuehEvent.instance.OpenEvent();
            BGM_Player.Instance.PlayEventBGM();
        }
        else if (ert == EnumRunTarget.Fight)
        {
            Fight();
            BGM_Player.Instance.PlayNormalBGM();
        }
    }

    public void EndGame()
    {

        PRole.Heal(1000);
        PHPImg.rectTransform.DOScaleY(9, 5f).SetEase(Ease.InExpo); ;
        PHPImg.DOColor(Color.white , 5f).SetEase(Ease.InExpo); ;
        StartCoroutine(ShowEndGameDialog());
    }
 
    IEnumerator ShowEndGameDialog()
    {
        yield return new WaitForSeconds(5f);
        PHPDialog.SetActive(true);
    }

    int quitCount = 0;
    public void ExitApplication()
    {
        quitCount++;
        YuehPlayerData.instance.ShowDialog("國瑜的話 能信嗎?");
        if(quitCount > 10)
        {
            YuehPlayerData.instance.ShowDialog("按Alt + F4啦");
            Application.Quit();
        }
    }

    public enum EnumRunTarget
    {
        Event,
        Camp,
        Fight
    }

    public void Run(EnumRunTarget ert) //開始走路 3秒後遇到選戰鬥或是休息
    {
        m_PlayerState = PlayerState.Run;
        YuehLoopBG.instance.DoMove(true);
        PRole.MyAni.SetTrigger("Run");
        float walkTime = Random.Range(1f, 3f);
        RunToEventTimer = new MyTimer(walkTime, () => { StartRandEvent(ert); }, true, false);
    }

    bool isEnemyStart = false;
    public void ResetEnemy()
    {
        isEnemyStart = false;
        EnemyAttackTimer = null;
        ERole.Reborn();
    }

    public void LearnTwoSkill()
    {
        LearnSkillByColor(SkillColor.All);
        LearnSkillByColor(SkillColor.All);
    }

    public void AfterFight() //怪物死掉
    {
        //學技能
        if (stageNow <= 4)
        {
            LearnTwoSkill();
        }
        m_PlayerState = PlayerState.None;
        SkillBar.Instance.ChangeAllOkBtnColor(false);
        HideEnemy();
        StartCoroutine(AfterFightWait());
    }

    void HideEnemy()
    {
        BattleAni.SetTrigger("Hide");
        EnemyHPUI.SetActive(false);
    }

    IEnumerator AfterFightWait()
    {
        yield return new WaitForSeconds(1);
        Run(EnumRunTarget.Event);
    }

    

    public int[] stages = new int[] { 1, 1, 1, 1, 2, 1, 1, 1, 2 , 0 , 0, 3}; //1哥不林 0鍋牛 2超派 3女巫
    public void Fight()
    {
        ResetEnemy();
        ResetPlayer();

        ERole.SetNewRole(stageNow);
        ERole.SetHP(stages[stageNow]);

        ERole.Reborn();

        EnemyHPUI.SetActive(true);
        BattleAni.SetTrigger("Debut");

        if(stageNow == 0)
        {
            DialogueManager.Instance.PlayDialogue("GoblinFirstTime", EnemyStartFight , _isNoSelect:true);
        }
        else if (stageNow == 1)
        {
            DialogueManager.Instance.PlayDialogue("GoblinSecondTime", GoblinSecondTime);
        }
        else if (stageNow == 4)
        {
            DialogueManager.Instance.PlayDialogue("AngryGoblinFirstTime", EnemyStartFight, _isNoSelect: true);
        }
        else if (stageNow == 9)
        {
            DialogueManager.Instance.ChangeBtnText();
            DialogueManager.Instance.PlayDialogue("gunuFirstTime", ()=> { gunuFirstTime(true); } , () => { gunuFirstTime(false);});
        }
        else if (stageNow == 10) //選蝸牛
        {
            DialogueManager.Instance.PlayDialogue("gunuBoss", EnemyStartFight, _isNoSelect: true);
            isGameEnd = true;
        }
        else if (stageNow == 11) //選女巫
        {
            DialogueManager.Instance.PlayDialogue("WitchBoss", EnemyStartFight, _isNoSelect: true);
            isGameEnd = true;
        }
        else
        {
            EnemyStartFight();
        }


        stageNow += 1;
    }

    void gunuFirstTime(bool isTrustWuPo) //相信巫婆 打蝸牛
    {
        AfterFight();
        if (isTrustWuPo == false) stageNow++;
    }

    bool isGoblinSecondTime = false; //是否罵幹
    void GoblinSecondTime()
    {
        isGoblinSecondTime = true;
        SkillBar.Instance.OnSkillClicked();
        
    }

    public void EnemyStartFight()
    {
        if (isEnemyStart) return;

        isEnemyStart = true;
        StartCoroutine(_EnemyStartAction());
    }

    void ResetPlayer()
    {
        list_playerUseIndexAction = new List<int>();
        m_PlayerState = PlayerState.Fight;
        PlayerActionCount = 0;
        skill16Coin = 0;
        skill12Count = 1;
        PRole.MyAni.SetTrigger("Idle");
        SkillBar.Instance.ChangeAllOkBtnColor(true);
    }

    IEnumerator _EnemyStartAction()
    {
        yield return new WaitForSeconds(0f);
        CurAIIndex = Random.Range(0, EnemyAI.Count);
        EnemyStartAction();
    }

    public int GetNextEnemy()
    {
        if(stages.Length > stageNow)
        {
            return stages[stageNow];
        }
        return 0;
    }

    public void Update()
    {
        if (PRole.HP <= 0) return;
        if (isC8763Attacking) return;

        if (PlayerCDTimer != null)
            PlayerCDTimer.RunTimer();
        if (PlayerAttackTimer != null)
            PlayerAttackTimer.RunTimer();
        if (RunToEventTimer != null)
            RunToEventTimer.RunTimer();
        if (EnemyAttackTimer != null && !ERole.isDie)
            EnemyAttackTimer.RunTimer();
        if (PlayerSleepTimer != null)
            PlayerSleepTimer.RunTimer();
        if (PlayerSkill8Timer != null)
            PlayerSkill8Timer.RunTimer();
        if (PlayerSkill9Timer != null)
            PlayerSkill9Timer.RunTimer();
        if (PlayerSkill14Timer != null)
            PlayerSkill14Timer.RunTimer();



        CDUPDate();
    }
    PlayerAction CurPlayerAction = null;
    GameObject C8763Go = null;
    bool ISC8763Go = false;

    GameObject goSkill8 = null;
    bool isSkill8 = false;
    List<int> list_playerUseIndexAction; //每一回合會把使用過的技能加進來
    public void TakeAction(int _index)
    {
        bool isTakeActionSucucss = true;
        PlayerActionCount++;

        isClickSkill = false;
        CurPlayerAction = Actions[_index].GetCopy();

        if(CurPlayerAction.MyAction != ActionType.Attack)
        {
            if (isGoblinSecondTime && !isGoblinSayE04 && stageNow == 2)
            {
                DialogueManager.Instance.PlayDialogue("GoblinSelect2", null);
            }
        }

        switch (CurPlayerAction.MyAction)
        {
            case ActionType.Attack:
                if (CurPlayerAction.Values[2] > 0)
                {
                    if (CurPlayerAction.Name == "C8763")
                    {
                        ISC8763Go = true;
                        C8763Go = VfxHandler.Instance.PlayVFX(Effect.charge, PRole.transform.position + new Vector3(0, 5, 0), Quaternion.identity, 10f);
                    }
                    PlayerAttackTimer = new MyTimer(CurPlayerAction.Values[2], UseC8763, true, false);
                    PRole.MyAni.SetTrigger("Charging");
                }
                else
                {
                    StartAttack();
                }
                break;
            case ActionType.Heal:
                Heal((int)CurPlayerAction.Values[0]);
                break;
            case ActionType.Sleep:
                StartSleep();
                break;
            case ActionType.Special:
                if (CurPlayerAction.SkillID == 8) //續力
                {
                    StartSkill8();
                }
                else if (CurPlayerAction.SkillID == 9) //聖光壟罩
                {
                    
                    if(list_playerUseIndexAction.Contains(_index)) //是否使用過此Index
                    {
                        int tempCount = list_playerUseIndexAction.Count;
                        //是否兩回合內有重複使用此Index
                        if(tempCount == 1)
                        {
                            if (list_playerUseIndexAction[tempCount - 1] == _index)
                            {
                                StartSkill9(false);
                                isTakeActionSucucss = false;
                            }
                        }
                        else if(tempCount == 2)
                        {
                            if(list_playerUseIndexAction[tempCount - 2] == _index)
                            {
                                StartSkill9(false);
                                isTakeActionSucucss = false;
                            }
                        }
                        else if (list_playerUseIndexAction[tempCount - 1] == _index || list_playerUseIndexAction[tempCount - 2] == _index)
                        {
                            StartSkill9(false);
                            isTakeActionSucucss = false;
                        }
                        else
                        {
                            StartSkill9();
                        }
                    }
                    else
                    {
                        StartSkill9();
                    }
                    
                }
                else if (CurPlayerAction.SkillID == 10) //狂暴
                {
                    StartSkill10();
                }
                else if (CurPlayerAction.SkillID == 11) //喜歡在後面
                {
                    SetPlayerCD(CurPlayerAction.Values[0]);
                }
                else if (CurPlayerAction.SkillID == 14) //羈絆
                {
                    StartSkil14();
                }

                break;
        }

        AfterTakeAction();

        if(isTakeActionSucucss) list_playerUseIndexAction.Add(_index);
    }

    void AfterTakeAction() //攻擊後
    {
        if(isSkill10)
        {
            if(CurPlayerAction.SkillID == 10)
            {
                skill10Count = 0;
            }
            else
            {
                skill10Count++;
            }
            if(skill10Count >=3)
            {
                isSkill10 = false;
                if (goSkill10 != null) Destroy(goSkill10);
            }
        }
        if(CurPlayerAction.SkillID == 12)
        {
            //香取神到流
            skill12Count++;
        }
        else
        {
            skill12Count = 1;
        }
        List<PlayerAction> tempList = new List<PlayerAction>(Actions);
        bool isDirty = false;
        for (int i = 0; i < tempList.Count; i++)
        {
            if(Actions[i].SkillID == 16)
            {
                PlayerAction pa = GetSkill(17);
                Actions[i] = pa;
                isDirty = true;
            }
            else if (Actions[i].SkillID == 17)
            {
                PlayerAction pa = GetSkill(16);
                Actions[i] = pa;

                isDirty = true;
            }
        }
        if(isDirty)
        {
            Refresh();
        }
    }

    public bool isSkill14;
    GameObject goSkill14 = null;
    void StartSkil14()
    {
        isSkill14 = true;
        if(goSkill14 != null) Destroy(goSkill14);
        SoundHandler.Instance.PlaySFX(Sound.Charge);
        goSkill14 = VfxHandler.Instance.PlayVFX(Effect.protect, PRole.transform.position + new Vector3(0, 5, 0), Quaternion.identity, 10f);
        SetPlayerCD(CurPlayerAction.Values[1]);
        PlayerSkill14Timer = new MyTimer(CurPlayerAction.Values[0], EndSkill14, null, true, false);
    }
    void EndSkill14()
    {
        isSkill14 = false;
        Destroy(goSkill14);
        PlayerSkill14Timer = null;
    }

    public int skill12Count; //香取神

    public bool isSkill10;
    public int skill10Count;
    GameObject goSkill10 = null;
    void StartSkill10()
    {
        //狂暴
        isSkill10 = true;
        if (goSkill10 != null) Destroy(goSkill10);
        SoundHandler.Instance.PlaySFX(Sound.Charge);
        goSkill10 = VfxHandler.Instance.PlayVFX(Effect.charge, PRole.transform.position + new Vector3(0, 5, 0), Quaternion.identity, 10f);
        SetPlayerCD(CurPlayerAction.Values[1]);
    }

    void StartSkill9(bool isCanUse = true)
    {
        //聖光壟罩
        if(isCanUse)
        {
            PlayerSkill9Timer = new MyTimer(2.5f, EndSkill9, null, true, false);
            isSkill9 = true;
            if (goSkill9 != null) Destroy(goSkill9);
             goSkill9 = VfxHandler.Instance.PlayVFX(Effect.protect2, PRole.transform.position + new Vector3(0, 5, 0), Quaternion.identity, 10f);
            SoundHandler.Instance.PlaySFX(Sound.Charge);
        }
        else
        {
            YuehPlayerData.instance.ShowDialog("兩次行動內無法再次施展此技能");
        }
        
        SetPlayerCD(CurPlayerAction.Values[1]);
    }

    bool isSkill9 = false;
    GameObject goSkill9 = null;
    void EndSkill9()
    {
        PlayerSkill9Timer = null;
        isSkill9 = false;
        Destroy(goSkill9);
    }

    
    void StartSkill8()
    {
        if (isSkill8)
        {
            EndSkill8();
        }

        isSkill8 = true;
        SoundHandler.Instance.PlaySFX(Sound.Charge);
        goSkill8 = VfxHandler.Instance.PlayVFX(Effect.charge, PRole.transform.position + new Vector3(0, 5, 0), Quaternion.identity, 10f);
        PlayerSkill8Timer = new MyTimer(15, EndSkill8, UpdateSkill8, true, false);
        SetPlayerCD((float)CurPlayerAction.Values[6]);
    }
    void EndSkill8()
    {
        isSkill8 = false;
        skill8Timer = 0;
        PlayerSkill8Timer = null;
        if (goSkill8 != null) Destroy(goSkill8);
        powerOfSkill8 = 1;
        PRole.MyAni.SetTrigger("Idle");
        PlayerCDTimer = null;
        PlayerSleepTimer = null;
    }

    void StartSleep()
    {
        isSleeping = true;
        PlayerSleepTimer = new MyTimer(99999, EndSleep, SleepRecoverHP, true, false);
        PRole.MyAni.SetTrigger("Sleep");
    }
    void EndSleep()
    {
        isSleeping = false;
        PRole.MyAni.SetTrigger("SleepUp");
        recoveryIntervalCout = 0;
        SetPlayerCD(0.2f);
        PlayerSleepTimer = null;
    }
    float recoveryIntervalCout = 0;
    void SleepRecoverHP() //睡覺會一直Update
    {
        recoveryIntervalCout += Time.deltaTime;
        if (recoveryIntervalCout > CurPlayerAction.Values[2])
        {
            SoundHandler.Instance.PlaySFX(Sound.Drink);
            VfxHandler.Instance.PlayVFX(Effect.smallExplosion, PRole.transform.position + new Vector3(0, 3, 0), Quaternion.identity, 3f);
            int lossHP = PRole.MaxHP - PRole.HP;
            int recovery = (int)(lossHP * CurPlayerAction.Values[0]);   
            if (recovery < CurPlayerAction.Values[1])
                recovery = (int)CurPlayerAction.Values[1];
            PRole.Heal(recovery);
            recoveryIntervalCout = 0;
            if (PRole.HPRatio >= 1)
            {
                EndSleep();
            }
        }
    }

    float skill8Timer = 0;
    void UpdateSkill8() 
    {
        skill8Timer += Time.deltaTime;

        powerOfSkill8 = 2;

        if (skill8Timer >= 5)
        {
            powerOfSkill8 = 3;
        }
        if (skill8Timer >= 10)
        {
            powerOfSkill8 = 4;
        }
    }


    private int skill16Coin;
    bool isC8763Attacking;
    void UseC8763()
    {
        if (ISC8763Go)
        {
            SoundHandler.Instance.PlaySFX(Sound.C8763);
            isC8763Attacking = true;
            StartCoroutine(WaitC8763Sound());
        }
    }

    IEnumerator WaitC8763Sound()
    {
        yield return new WaitForSeconds(4f);
        isC8763Attacking = false;
        StartAttack();
    }

    void StartAttack()
    {
        float atk = CurPlayerAction.Values[0];
        if(CurPlayerAction.SkillID == 12)
        {
            atk *= skill12Count;
        }
        if (CurPlayerAction.SkillID == 16)
        {
            skill16Coin++;
        }
        if (CurPlayerAction.SkillID == 17)
        {
            atk = Random.Range(5,60);
            CurPlayerAction.Values[1] = skill16Coin;
            skill16Coin = 0;
        }

        PRole.MyAni.speed = 1.5f * CurPlayerAction.Values[1]; ;
        CurPlayerAction.Values[1]--;
        if ((int)CurPlayerAction.Values[1] >= 0)
        {
            PAttack((int)atk);
            if (isSkill8 && CurPlayerAction.SkillID != 8)
            {
                EndSkill8();
            }
            PlayerAttackTimer = new MyTimer(0.2f, StartAttack, true, false);
        }
        else
        {
            PlayerAttackTimer = null;
            //開始CD
            SetPlayerCD((float)CurPlayerAction.Values[3]);
            PRole.MyAni.speed = 1;
        }
        ISC8763Go = false;
        
    }
    void SetPlayerCD(float _time)
    {
        _time = CDBase * _time;
        //開始CD
        PlayerCDTimer = new MyTimer(_time, EndCD, true, false);

    }
    void CDUPDate()
    {
        if (PlayerCDTimer != null)
        {
            CDImg.fillAmount = (PlayerCDTimer.CircleTime - PlayerCDTimer.CurTimer) / PlayerCDTimer.CircleTime;
        }
        else
            CDImg.fillAmount = 1;
    }
    void EndCD()
    {
        PlayerCDTimer = null;
    }

    bool isGoblinSayE04 = false;
    public void PAttack(int _value)
    {
        if (isGoblinSecondTime && !isGoblinSayE04)
        {
            isGoblinSayE04 = true;
            DialogueManager.Instance.PlayDialogue("GoblinSelect", null);
            EnemyStartFight();
        }

        if (CurPlayerAction.SkillID == 15)
        {
            //飽滿打擊
            if (PRole.HPRatio > 0.8f)
            {
                _value = (int)(PRole.HP * 0.6f);
                CurPlayerAction.Values[3] = 1.5f;
            }
            else if (PRole.HPRatio > 0.5f)
            {
                _value = (int)(PRole.HP * 0.4f);
                CurPlayerAction.Values[3] = 1.2f;
            }
            else 
            {
                _value = (int)(PRole.HP * 0.2f);
                CurPlayerAction.Values[3] = 1f;
            }
        }

        _value = _value * powerOfSkill8;
        PRole.MyAni.SetTrigger("Attack");
        if (ERole.IsDefend)
        {
            SoundHandler.Instance.PlaySFX(Sound.Block);
            if (isReDamage)
            {
                //被反傷
                PRole.TakeDmg((int)(_value * 0.5f));
            }
        }
        else
        {
            if(CurPlayerAction.SkillID == 13)
            {
                //精準打擊 不用CD
                CurPlayerAction.Values[3] = 0;
            }
            
            //播放特效
            if(CurPlayerAction.SkillID == 3 || CurPlayerAction.SkillID == 4 || CurPlayerAction.SkillID == 7 || CurPlayerAction.SkillID == 16 || CurPlayerAction.SkillID == 17) //c8763 火球 coinMaster
            {
                PlaySFX(Effect.HitEnemy2, false);
                SoundHandler.Instance.PlaySFX(Sound.Attack);
            }
            else
            {
                PlaySFX(Effect.HitEnemy, false);
                SoundHandler.Instance.PlaySFX(Sound.HeavyAttack);
            }
            
            ERole.TakeDmg(_value);
            Debug.Log("傷害 : " + _value.ToString());


            if (isSavePowerEnemy) //蓄力
            {
                damageBreakAction += _value;
                if (CurEnemyAction.Values.Count >= 5)
                {
                    if(damageBreakAction >= CurEnemyAction.Values[4]) //超過傷害 打斷Action
                    {
                        Debug.Log("超過傷害 打斷Action");
                        PlaySFX(Effect.HitEnemyBreak, false);
                        SoundHandler.Instance.PlaySFX(Sound.EnemyBreak);
                        CurAIActionIndex = EnemyAI[CurAIIndex].Count;
                        ERole.MyAni.SetTrigger("Idle");
                        damageBreakAction = 0;
                        isSavePowerEnemy = false;
                        EnemyAttackTimer = new MyTimer(1, EnemyStartAction, true, false);
                    }
                }
            }
        }
    }
    public void EAttack(int _value)
    {
        ERole.MyAni.SetTrigger("Attack");
        //SoundHandler.Instance.PlaySFX(Sound.Attack);


        if (isSkill9)
        {
            SoundHandler.Instance.PlaySFX(Sound.Block);
            return;
        }

        if(isSkill14)
        {
            EndSkill14();
            SoundHandler.Instance.PlaySFX(Sound.Block);
            return;
        }

        PRole.TakeDmg(_value);
        

        //播放特效 超派蓄力打之類高傷的
        if (CurEnemyAction.SkillID == 7 || CurEnemyAction.SkillID == 6 || CurEnemyAction.SkillID == 14 || CurEnemyAction.SkillID == 16 || CurEnemyAction.SkillID == 3) 
        {
            PlaySFX(Effect.HitEnemy2, true);
            SoundHandler.Instance.PlaySFX(Sound.Attack);
        }
        else
        {
            PlaySFX(Effect.HitEnemy, true);
            SoundHandler.Instance.PlaySFX(Sound.HeavyAttack);
        }
        
        


        if (ISC8763Go) //被怪物中斷
        {
            //開始CD
            SetPlayerCD((float)CurPlayerAction.Values[3]);
            if (C8763Go != null) Destroy(C8763Go);
            PlayerAttackTimer = null;
            ISC8763Go = false;
            isC8763Attacking = false;
            PRole.MyAni.SetTrigger("Idle");
        }

        if(isSleeping) //睡覺被中斷
        {
            EndSleep();
        }
    }
    public void Heal(int _value)
    {
        PRole.Heal(_value);
        VfxHandler.Instance.PlayVFX(Effect.heal, PRole.transform.position + new Vector3(0, 5, 0), Quaternion.identity, 0.8f);
        SoundHandler.Instance.PlaySFX(Sound.Drink);
        PRole.MyAni.SetTrigger("Heal");
        //開始CD
        SetPlayerCD((float)CurPlayerAction.Values[1]);

        if(CurPlayerAction.SkillID == 18)
        {
            //臨機應變
            SetOneRandomSkill();
        }
    }
    public void UpdateHealthUI()
    {
        PHPImg.fillAmount = PRole.HPRatio;
        EHPImg.fillAmount = ERole.HPRatio;
    }
    public void EnemyStartAction() //重新骰行動 CD1秒
    {
        EnemyAttackTimer = new MyTimer(1, EnemyAction, true, false);
    }
    int CurAIIndex = 0;
    int CurAIActionIndex = 0;
    public void EnemyAction() //1哥不林 0鍋牛 2超派 3女巫
    {
        EnemyAttackTimer = null;
        if (CurAIActionIndex >= EnemyAI[CurAIIndex].Count)//新行動
        {
            CurAIActionIndex = 0;

            CurAIIndex = Random.Range(0, EnemyAI.Count);


            EnemyAttackTimer = new MyTimer(1, EnemyAction, true, false);
           
        }
        else
        {
            EnemyAttackTimer = new MyTimer(0.5f, ()=> { LagSkill(CurAIIndex, CurAIActionIndex); }, true, false);
        }
        
    }

    public void LagSkill(int _CurAIIndex , int _CurAIActionIndex)
    {
        EnemyTakeAction(EnemyAI[_CurAIIndex][_CurAIActionIndex]);
        CurAIActionIndex++;

    }

    public bool isSleeping = false;
    public bool isClickSkill = false; //按下使用技能 到施放之前都是True
    public bool IsCanUseSkill()
    {
        if(PlayerCDTimer != null) //還在CD
        {
            YuehPlayerData.instance.ShowDialog("CD中...");
            return false;
        }
        if(PlayerAttackTimer != null)
        {
            YuehPlayerData.instance.ShowDialog("攻擊中...");
            return false;
        }
        if(isClickSkill)
        {
            return false;
        }
        if(isSleeping)
        {
            YuehPlayerData.instance.ShowDialog("睡覺中...");
            return false;
        }

        return true;
    }

    public bool IsHaveAttackAction()
    {
        foreach (PlayerAction pa in BattleManager.instance.Actions)
        {
            if (pa.MyAction == ActionType.Attack)
            {
                return true;
            }
        }
        return false;
    }

    public void PlaySFX(Effect effect , bool isPlayer)
    {
        Transform target = PRole.transform;
        if (!isPlayer) target = ERole.transform;

        VfxHandler.Instance.PlayVFX(effect, target.position + new Vector3(0, 5, 0), Quaternion.identity, 3f);
    }


}