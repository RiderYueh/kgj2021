using UnityEngine;

public class EnemyRole : Role
{
    public bool IsDefend = false;
    public Animator[] Anis = null;
    public void Defend(bool isReDamage = false)
    {
        Debug.Log("IsDefend");
        IsDefend = true;
        if (isReDamage)
        {
            MyAni.SetTrigger("ReDamage");
        }
        else
        {
            MyAni.SetTrigger("Defend");
        }    
        
    }
    public void EndDefend()
    {
        IsDefend = false;
        MyAni.SetTrigger("Idle");
    }


    public override void OnDeath()
    {
        if (isDie) return;
        isDie = true;
        BattleManager.instance.AfterFight();
    }

    public void RandomNewRole()
    {
        int index = Random.Range(0, Anis.Length);
        MyAni = Anis[index];
        for(int i=0;i< Anis.Length;i++)
        {
            Anis[i].gameObject.SetActive(false);
            BattleManager.instance.InitEnemySkill(i);
        }
        Anis[index].gameObject.SetActive(true);
    }
    

    public void SetNewRole(int value)
    {
        int index = 0;
        if (BattleManager.instance.stages.Length > value)
        {
            index = BattleManager.instance.stages[value];
        }
        
        MyAni = Anis[index];
        for (int i = 0; i < Anis.Length; i++)
        {
            Anis[i].gameObject.SetActive(false);
        }
        BattleManager.instance.InitEnemySkill(index);
        Anis[index].gameObject.SetActive(true);
    }

    public void SetHP(int value)//1�����L 0��� 2�W�� 3�k��
    {
        if (value == 1)
        {
            MaxHP = 93;
        }
        else if (value == 0)
        {
            MaxHP = 690;
        }
        else if (value == 2)
        {
            MaxHP = 340;
        }
        else if (value == 3)
        {
            MaxHP = 500;
        }
    }
}