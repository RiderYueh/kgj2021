using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum ActionType
{
    Attack,//攻擊力,攻擊次數,續力時間,CD
    Heal,//攻擊力,CD
    Sleep,//每秒回損失血量百分比,最低回血
    Block,//防禦
    Special,//特殊
    EnemySpecial,
}

public class PlayerAction
{
    public ActionType MyAction;
    public List<float> Values;
    public string Name = "";
    public int SkillID;
    public string Description = "";


    public PlayerAction GetCopy()
    {
        PlayerAction pa = (PlayerAction)this.MemberwiseClone();
        pa.Values = new List<float>(this.Values);
        return pa;
    }
}
