using UnityEngine;

public class PlayerRole : Role
{
    public override void OnDeath()
    {
        if (isDie) return;
        isDie = true;
        SceneCutter.Instance.LoadScene("OldmanBoxBox", 3);
    }

}