using UnityEngine;

public class Role : MonoBehaviour
{
    public bool isDie = false;
    int hp = 0;
    public int HP
    {
        get
        {
            return hp;
        }
        set
        {
            if (value < 0)
                hp = 0;
            else if (value > MaxHP)
                hp = MaxHP;
            else
                hp = value;
            Death();
            BattleManager.instance.UpdateHealthUI();
        }
    }
    public int MaxHP;
    public float HPRatio
    {
        get
        {
            return (float)HP / (float)MaxHP;
        }
    }

    public virtual bool Death()
    {
        if (HP <= 0)
        {
            SoundHandler.Instance.PlaySFX(Sound.bigExplosion);
            VfxHandler.Instance.PlayVFX(Effect.bigExplosion, transform.position + new Vector3(0, 0, 0), Quaternion.identity, 10f);
            OnDeath();
            return true;
        }
        return false;
    }

    public virtual void OnDeath()
    {
        
    }

    public void InitRole()
    {
        isDie = true;
    }

    public Animator MyAni = null;

    public void TakeDmg(int _value)
    {
        HP -= _value;
    }
    public void Heal(int _value)
    {
        HP += _value;
    }

    
    public void Reborn()
    {
        HP = MaxHP;
        isDie = false;
    }
}