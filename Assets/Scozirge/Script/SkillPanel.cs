using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillPanel : MonoBehaviour
{
    public GameObject SkillPrefab = null;
    List<PlayerAction> Actions;
    List<GameObject> GoList = new List<GameObject>();
    public void SetSkill(List<PlayerAction> _actions)
    {
        Actions = _actions;
        GoList.Clear();

        for (int i=0;i< Actions.Count;i++)
        {
            GameObject go = Instantiate(SkillPrefab, transform);
            go.GetComponentInChildren<Text>().text = Actions[i].Name;
            GoList.Add(go);
        }
    }
    
}