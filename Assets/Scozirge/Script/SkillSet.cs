using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Scoz.Func;
using System.Linq;

public partial class BattleManager : MonoBehaviour
{
    int TotalSkill = 17;
    public List<int> list_CanUseSkill;
    public PlayerAction GetSkill(int _value)
    {
        PlayerAction pa = new PlayerAction();
        pa.SkillID = _value;
        //1 普通攻擊
        //2 快速攻擊
        //3 強力攻擊
        //4 C8763
        //5 回復藥水
        //6 睡覺
        if (_value == 1)
        {
            pa.Values = new List<float>() { 15, 1, 0, 1 };//攻擊力,攻擊次數,續力時間,CD
            pa.MyAction = ActionType.Attack;
            pa.Name = "普通攻擊";
            pa.Description = "普通的打1下。";
            
        }
        else if (_value == 2)
        {
            pa.MyAction = ActionType.Attack;
            pa.Name = "快速攻擊";
            pa.Description = "快速的戳1下，不是很痛。";
            pa.Values = new List<float>() { 7, 1, 0, 0.5f };//攻擊力,攻擊次數,續力時間,CD

        }
        else if (_value == 3)
        {
            pa.MyAction = ActionType.Attack;
            pa.Name = "強力攻擊";
            pa.Description = "好用力的砍1下，可是好慢。";
            pa.Values = new List<float>() { 24, 1, 0, 2 };//攻擊力,攻擊次數,續力時間,CD
 
        }
        else if (_value == 4)
        {
            pa.MyAction = ActionType.Attack;
            pa.Name = "C8763";
            pa.Description = "原地擺出帥氣pose10秒之後，攻擊目標16次。擺pose時被攻擊則中斷動作。";
            pa.Values = new List<float>() { 22, 16, 10, 1 };//攻擊力,攻擊次數,續力時間,CD

        }
        else if (_value == 5)
        {
            pa.MyAction = ActionType.Heal;
            pa.Name = "回復藥水";
            pa.Description = "勇者必備。";
            pa.Values = new List<float>() { 20, 1 };//補血,CD

        }
        else if (_value == 6)
        {
            pa.MyAction = ActionType.Sleep;
            pa.Name = "睡覺";
            pa.Description = "躺下來睡覺，逐漸回復生命值，直到生命值全滿，或是遭受攻擊。";
            pa.Values = new List<float>() { 0.4f, 8 , 1f};//每秒回損失血量百分比,最低回血,每幾秒一次
        }
        else if (_value == 7)
        {
            pa.MyAction = ActionType.Attack;
            pa.Name = "火球術";
            pa.Description = "原地射出一顆火球攻擊敵人。長年練習，左手會很粗壯。";
            pa.Values = new List<float>() { 10, 1, 0, 0.5f };//攻擊力,攻擊次數,續力時間,CD
        }
        else if (_value == 8)
        {
            pa.MyAction = ActionType.Special;
            pa.Name = "蓄力";
            pa.Description = "原地蓄力共15秒，於5/10/15秒內進行下次攻擊時，傷害變為2/3/4倍。";
            pa.Values = new List<float>() {5,10,15,2,3,4,0.5f}; // 秒,秒,秒,倍,倍,倍,CD
        }
        else if (_value == 9)
        {
            pa.MyAction = ActionType.Special;
            pa.Name = "聖光壟罩";
            pa.Description = "產生一個免疫傷害的護盾持續2.5秒。之後兩次行動無法再次施展";
            pa.Values = new List<float>() {2.5f , 0.5f}; // 持續時間,cd
        }
        else if (_value == 10)
        {
            pa.MyAction = ActionType.Special;
            pa.Name = "狂暴";
            pa.Description = "之後3次行動只會選擇「攻擊」類型技能。";
            pa.Values = new List<float>() { 3,0.5f }; // 次數 ,cd
        }
        else if (_value == 11)
        {
            pa.MyAction = ActionType.Special;
            pa.Name = "喜歡在後面";
            pa.Description = "被選中時無效果。前方的技能被選中的機率增加3倍。";
            pa.Values = new List<float>() { 0.5f}; // cd
        }
        else if (_value == 12)
        {
            pa.MyAction = ActionType.Attack;
            pa.Name = "香取神道流";
            pa.Description = "攻擊1次，連續施放「香取神道流」時傷害加倍。";
            pa.Values = new List<float>() { 8, 1, 0, 0.8f };//攻擊力,攻擊次數,續力時間,CD

        }
        else if (_value == 13)
        {
            pa.MyAction = ActionType.Attack;
            pa.Name = "精準打擊";
            pa.Description = "攻擊1次，如果成功造成傷害，冷卻歸零。";
            pa.Values = new List<float>() { 18, 1, 0, 4f };//攻擊力,攻擊次數,續力時間,CD

        }
        else if (_value == 14)
        {
            pa.MyAction = ActionType.Special;
            pa.Name = "羈絆";
            pa.Description = "召喚一名夥伴，幫忙抵擋1次攻擊，持續5秒。";
            pa.Values = new List<float>() { 5, 1.5f };//格檔時間,CD

        }

        else if (_value == 15)
        {
            pa.MyAction = ActionType.Attack;
            pa.Name = "飽滿斬擊";
            pa.Description = "生命值越多，打得越痛。";
            pa.Values = new List<float>() { 1, 1, 0, 1.5f };//攻擊力,攻擊次數,續力時間,CD (數值都寫在battleManager)
        }
        else if (_value == 16)
        {
            pa.MyAction = ActionType.Attack;
            pa.Name = "Coin Master";
            pa.Description = "任何行動後，會切換成幸運輪盤。CM:攻擊1次，獲取1枚硬幣。幸運輪盤:消耗所有硬幣，每枚硬幣造成1次隨機數值攻擊。";
            pa.Values = new List<float>() { 8, 1, 0, 1 };//攻擊力,攻擊次數,續力時間,CD
        }
        else if (_value == 17)
        {
            pa.MyAction = ActionType.Attack;
            pa.Name = "幸運輪盤";
            pa.Description = "任何行動後，會切換成Coin Master。CM:攻擊1次，獲取1枚硬幣。幸運輪盤:消耗所有硬幣，每枚硬幣造成1次隨機數值攻擊。";
            pa.Values = new List<float>() { 60, 1, 0, 2 };//攻擊力,攻擊次數,續力時間,CD 在battle裡有直接設定
        }
        else if (_value == 18)
        {
            pa.MyAction = ActionType.Heal;
            pa.Name = "臨機應變";
            pa.Description = "將隨機一項技能替換成另一隨機技能，回復些許生命值。";
            pa.Values = new List<float>() { 8, 1 };//補血,CD
        }
        return pa;
    }
    public void Refresh()
    {
        SkillBar.Instance.InitLabels(Actions.Count);

        string[] Names = new string[Actions.Count];
        int[] skills = new int[Actions.Count];


        for (int i = 0; i < Actions.Count; i++)
        {
            Names[i] = Actions[i].Name;
            skills[i] = Actions[i].SkillID;

            if (YuehPlayerData.instance.lockSkillCount >= 1)
            {
                if(i == 0) Names[i] = "已借出";
            }
            if (YuehPlayerData.instance.lockSkillCount >= 2)
            {
                if (i == 1) Names[i] = "已借出";
            }

        }
        SkillBar.Instance.InitSkills(Names , skills);
        SkillBar.Instance.SetEndFunction(TakeAction);
    }
    void InitPlayerSkill()
    {
        list_CanUseSkill = new List<int>();

        LearnSkill(1, false);
        LearnSkill(2, false);
        LearnSkill(3, false);
        LearnSkill(5, false);
        LearnSkill(6, false);

        /*LearnSkill(1);
        LearnSkill(2);
        LearnSkill(3);
        LearnSkill(5);
        LearnSkill(6);*/
#if UNITY_EDITOR
        Actions.Add(GetSkill(1));
        Actions.Add(GetSkill(1));
        Actions.Add(GetSkill(1));
        Actions.Add(GetSkill(5));
        Actions.Add(GetSkill(6));
#else
        Actions.Add(GetSkill(1));
        Actions.Add(GetSkill(1));
        Actions.Add(GetSkill(1));
        Actions.Add(GetSkill(5));
        Actions.Add(GetSkill(6));
#endif
        Refresh();
    }

    public void GetRandomSkill(int count = 1)
    {
        for (int i = 0; i < count; i++)
        {
            int ran = Random.Range(0, list_CanUseSkill.Count);
            Actions.Add(GetSkill(list_CanUseSkill[ran]));
        }
        
        Refresh();
    }

    public void RandomAllSkill() //骰掉全部技能
    {
        int count = Actions.Count;
        Actions.Clear();

        GetRandomSkill(count);
    }

    public void SetOneRandomSkill()
    {
        //隨機替換一格技能
        int ran = Random.Range(0, Actions.Count);
        int ran2 = Random.Range(0 , list_CanUseSkill.Count);
        PlayerAction pa = GetSkill(list_CanUseSkill[ran2]);
        Actions[ran] = pa;

        Refresh();
    }

    public void LearnSkill(int skillID , bool isShowDialog = true)
    {
        if(!list_CanUseSkill.Contains(skillID))
        {
            list_CanUseSkill.Add(skillID);
            if(isShowDialog)
            {
                if(skillID == 4 || skillID == 12 || skillID == 16) //大招
                {
                    YuehPlayerData.instance.ShowDialogBig("學會了 : " + GetSkill(skillID).Name);
                }
                else
                {
                    YuehPlayerData.instance.ShowDialog("學會了 : " + GetSkill(skillID).Name);
                }
                
            }
        }
        else
        {
            if (isShowDialog)
                YuehPlayerData.instance.ShowDialog("本來就學會了 : " + GetSkill(skillID).Name);
        }    
    }

    public enum SkillColor
    {
        None,
        R,//女巫藥水
        G,//女巫藥水
        B,//女巫藥水
        All,//戰鬥後隨機學技能
    }

    public void LearnSkillByColor(SkillColor sc = SkillColor.None)
    {
        
        if(sc == SkillColor.None)
        {
            for (int i = 0; i < TotalSkill; i++)
            {
                if (!list_CanUseSkill.Contains(i))
                {
                    LearnSkill(i);
                    YuehPlayerData.instance.ShowDialog("學會了 : " + GetSkill(i).Name);
                    return;
                }
            }
            YuehPlayerData.instance.ShowDialog("全技能都會了");
        }

        else
        {
            int[] arrayR = new int[]{18,7,15};
            int[] arrayG = new int[]{10,8,11};
            int[] arrayB = new int[]{14,13,9};
            int[] arrayAll = new int[] { 18, 7, 15, 10, 8, 11, 14, 13, 9 };

            int[] tempArrar = null;
            if(sc == SkillColor.R)
            {
                tempArrar = arrayR;
            }
            else if (sc == SkillColor.G)
            {
                tempArrar = arrayG;
            }
            else if (sc == SkillColor.B)
            {
                tempArrar = arrayB;
            }
            else if (sc == SkillColor.All)
            {
                tempArrar = arrayAll;
            }

            List<int> list_arrayAll = new List<int>(tempArrar.ToList());

            foreach (int tempSkillID in list_CanUseSkill)
            {
                if (list_arrayAll.Contains(tempSkillID))
                {
                    list_arrayAll.Remove(tempSkillID);
                }
            }
            for (int i = 0; i < list_arrayAll.Count; i++)
            {
                //Debug.Log(list_arrayAll[i]);
            }
            RandomLearnSkillByList(list_arrayAll);
        }

    }

    private void RandomLearnSkillByList(List<int> _list)
    {
        if(_list.Count ==0)
        {
            Debug.Log("學會全部技能了");
            return;
        }
        int ran = Random.Range(0, _list.Count);
        LearnSkill(_list[ran]);

        CheckLearnPowerSkill();
    }

    private void CheckLearnPowerSkill() //學會三個顏色後 自動學會
    {
        if(!list_CanUseSkill.Contains(16)) //R CM
        {
            if (list_CanUseSkill.Contains(18) && list_CanUseSkill.Contains(7) && list_CanUseSkill.Contains(15))
            {
                LearnSkill(16);
            }
        }
        if (!list_CanUseSkill.Contains(12)) //G 香取
        {
            if (list_CanUseSkill.Contains(10) && list_CanUseSkill.Contains(18) && list_CanUseSkill.Contains(11))
            {
                LearnSkill(12);
            }
        }
        if (!list_CanUseSkill.Contains(4)) //B c8763
        {
            if (list_CanUseSkill.Contains(14) && list_CanUseSkill.Contains(13) && list_CanUseSkill.Contains(9))
            {
                LearnSkill(4);
            }
        }

    }
    
    
}