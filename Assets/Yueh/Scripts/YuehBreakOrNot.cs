using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class YuehBreakOrNot : MonoBehaviour
{
    public static YuehBreakOrNot instance;
    private void Awake() {
        if(instance == null)
            instance = this;
    }
    public GameObject goPlaceNow;
    public GameObject all;
    public GameObject all2;
    public GameObject goFightBG;
    public GameObject goBreakBG;
    public Image imageFadeInBG;
    public Text textInfo;

    public Text textOnBtn;

    public Image imageEnemy;
    public List<Sprite> list_enemySprite;



    int tempStage;
    public void GetNext() 
    {

        all.SetActive(true);
        imageFadeInBG.color = new Color(0,0,0,0);
        imageFadeInBG.DOFade(1, 1);
        
        RanEvent();

        StartCoroutine(Open());

        imageEnemy.sprite = list_enemySprite[BattleManager.instance.GetNextEnemy()];
    }

    IEnumerator Open()
    {
        yield return new WaitForSeconds(1.5f);
        all2.SetActive(true);
        //Debug.Log(-226 + (BattleManager.instance.stageNow * 51.2f));
        goPlaceNow.GetComponent<Image>().fillAmount = (BattleManager.instance.stageNow + 1f) / 10f;



        Refresh();
    }

    void RanEvent()
    {
        goFightBG.GetComponent<Image>().DOFade(0, 0.0f);
        goBreakBG.GetComponent<Image>().DOFade(0, 0.0f);
        goFightBG.SetActive(false);
        goBreakBG.SetActive(false);

        tempStage = Random.Range(0, 2);
        if (tempStage == 0)
        {
            goFightBG.SetActive(true);
            goFightBG.GetComponent<Image>().DOFade(1, 0.5f);
            textInfo.text = "目的地 :　戰鬥\n可消耗生命重骰目的地";
        }
        else
        {
            goBreakBG.SetActive(true);
            goBreakBG.GetComponent<Image>().DOFade(1, 0.5f);
            textInfo.text = "目的地 :　營地\n可消耗生命重骰目的地";
        }

        Refresh();
    }

    public void OnClickReDo()
    {
        SoundHandler.Instance.PlaySFX(Sound.Button);
        YuehPlayerData.instance.BreakOrNotCount += 1;
        BattleManager.instance.PRole.HP -= YuehPlayerData.instance.GetBreakOrNotCost();
        RanEvent();
    }

    void Refresh()
    {
        textOnBtn.text = "消耗"+YuehPlayerData.instance.GetBreakOrNotCost().ToString()+"點生命";
    }

    public void OnClickGo()
    {
        SoundHandler.Instance.PlaySFX(Sound.Button);
        YuehPlayerData.instance.BreakOrNotCount = 1;
        if (tempStage == 0)
        {
            Debug.Log("GO 下一關戰鬥");
            BattleManager.instance.Run(BattleManager.EnumRunTarget.Fight);
        }
        else
        {
            Debug.Log("GO 下一關營火");
            YuehCamp.instance.Open();
        }
        all.SetActive(false);
        all2.SetActive(false);
    }
}
