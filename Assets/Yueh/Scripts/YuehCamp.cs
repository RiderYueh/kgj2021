using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class YuehCamp : MonoBehaviour
{
    public static YuehCamp instance;
    public Text m_Text;
    public GameObject all;

    private void Awake()
    {
        instance = this;
    }

    public void Open()
    {
        all.SetActive(true);
        Refresh();
    }

    public void Refresh()
    {
        m_Text.text = "花費" + YuehPlayerData.instance.GetRanSkillCost().ToString() + "點HP重骰技能?";
    }

    public void OnClickOK()
    {
        SoundHandler.Instance.PlaySFX(Sound.Button);
        YuehPlayerData.instance.RanSkillCount += 1;
        BattleManager.instance.PRole.HP -= YuehPlayerData.instance.GetRanSkillCost();
        BattleManager.instance.RandomAllSkill();
        Refresh();
    }

    public void OnClickExit()
    {
        SoundHandler.Instance.PlaySFX(Sound.Button);
        all.SetActive(false);
        YuehPlayerData.instance.RanSkillCount = 1;
        BattleManager.instance.Run(BattleManager.EnumRunTarget.Fight);
    }
}
