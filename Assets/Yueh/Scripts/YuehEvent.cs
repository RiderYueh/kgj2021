using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class YuehEvent : MonoBehaviour
{
    public static YuehEvent instance;
    public GameObject goWupo;

    public GameObject goTrader;
    public Text textTrader;
    
    private void Awake() {
        if(instance == null) instance = this;
    }

    int wupoCount = 0;
    int eventTotalCount = 0;
    int traderCount = 0;

    bool isWupoFirst = false; //第一次戰鬥後遇到巫婆或是商人
    private void Start()
    {
        wupoCount = 0;
        traderCount = 0;
        eventTotalCount = 0;

        if(Random.Range(0,10) >= 5)
        {
            isWupoFirst = true;
        }
    }

    void VisitWupo()
    {
        goWupo.SetActive(true);
        if (wupoCount == 0)
        {
            DialogueManager.Instance.PlayDialogue("WitchFirstTime", WupoOnClickOK , NoDrink);
        }
        else
        {
            DialogueManager.Instance.PlayDialogue("WitchSecondTime", WupoOnClickOK , NoDrink);
        }
        wupoCount++;
    }

    void NoDrink()
    {
        YuehPlayerData.instance.ShowDialog("你一無所獲的離開了");
        OnClickExit();
    }

    void ExitTrader()
    {
        OnClickExit();
    }

    void TraderFirstTime()
    {
        DialogueManager.Instance.PlayDialogue("TraderFirstTime2", ExitTrader);
        YuehPlayerData.instance.ShowDialog("一格技能被封印");
        YuehPlayerData.instance.lockSkillCount++;
        BattleManager.instance.Refresh();
    }

    void TraderSecondTime()
    {
        DialogueManager.Instance.PlayDialogue("TraderSecondTime2", ExitTrader);
        YuehPlayerData.instance.ShowDialog("一格技能被封印");
        YuehPlayerData.instance.lockSkillCount++;
        BattleManager.instance.Refresh();
    }
    void TraderThirdTime()
    {
        TraderGift();
        DialogueManager.Instance.PlayDialogue("TraderThirdTime2", ExitTrader);
    }

    void TraderGift()
    {
        YuehPlayerData.instance.lockSkillCount = 0;
        YuehPlayerData.instance.ShowDialogBig("你的外觀發生了變化");
        YuehMenu.instance.OnBounsHead();
        BattleManager.instance.LearnSkillByColor(BattleManager.SkillColor.All);
        BattleManager.instance.Refresh();
    }


    void VisitTrader()
    {
        goTrader.SetActive(true);

        if(traderCount == 0)
        {
            DialogueManager.Instance.PlayDialogue("TraderFirstTime", TraderFirstTime , NoTrade);
        }
        if (traderCount == 1)
        {
            DialogueManager.Instance.PlayDialogue("TraderSecondTime", TraderSecondTime);
        }
        if (traderCount == 2)
        {
            DialogueManager.Instance.PlayDialogue("TraderThirdTime", TraderThirdTime);
        }
        traderCount++;
    }

    void NoTrade()
    {
        traderCount = 0;
        YuehPlayerData.instance.ShowDialog("你一無所獲的離開了");
        OnClickExit();
    }

    public void OpenEvent()
    {
        eventTotalCount += 1;

        if(traderCount >= 3)
        {
            VisitWupo();
        }
        else if (eventTotalCount % 2 == 0)
        {
            if(isWupoFirst)
            {
                VisitWupo();
            }
            else
            {
                VisitTrader();
            }
        }

        else
        {
            if (isWupoFirst)
            {
                VisitTrader();
            }
            else
            {
                VisitWupo();
            }
        }
    }

    public void WupoOnClickOK()
    {
        SoundHandler.Instance.PlaySFX(Sound.Button);
        int temp = Random.Range(0 , 3);
        YuehPlayerData.instance.List_Wupo.Add(temp);
        if(temp == 0)
        {
            YuehPlayerData.instance.ShowDialog("你喝下了紅藥水" , 3,3);
            YuehMenu.instance.OnChangeHeadColor(YuehMenu.EnumColor.Red);
        }
        else if(temp == 1)
        {
            YuehPlayerData.instance.ShowDialog("你喝下了綠藥水" ,3,3);
            YuehMenu.instance.OnChangeHeadColor(YuehMenu.EnumColor.Green);
        }
        else if(temp == 2)
        {
            YuehPlayerData.instance.ShowDialog("你喝下了藍藥水",3,3);
            YuehMenu.instance.OnChangeHeadColor(YuehMenu.EnumColor.Blue);
        }

        if(YuehPlayerData.instance.List_Wupo.Contains(1) && YuehPlayerData.instance.List_Wupo.Contains(2) && YuehPlayerData.instance.List_Wupo.Contains(3))
        {
            YuehMenu.instance.OnChangeHeadColor(YuehMenu.EnumColor.White);
            BattleManager.instance.GetRandomSkill(10);
            YuehPlayerData.instance.ShowDialogBig("體內的藥水混合，你獲得了10格技能格");
            YuehPlayerData.instance.List_Wupo.Clear();
        }
        else
        {
            BattleManager.instance.GetRandomSkill();
        }
        OnClickExit();
    }

    public void OnClickExit()
    {
        SoundHandler.Instance.PlaySFX(Sound.Button);
        goWupo.SetActive(false);
        goTrader.SetActive(false);
        GoNext();
    }

    public void GoNext()
    {
        Debug.Log("Evnet 結束");
        BattleManager.instance.Run(BattleManager.EnumRunTarget.Camp);
    }
}
