using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class YuehGetSkillPrefab : MonoBehaviour
{
    public Text m_Text;
    public void Init(string skillName , float ranTimeA = 3f , float ranTimeB = 5f)
    {
        m_Text.text = skillName;
        gameObject.SetActive(true);
        gameObject.transform.localPosition += new Vector3(0, Random.Range(-100,100), 0);
        transform.DOMoveY(700, Random.Range(ranTimeA, ranTimeB)).SetEase(Ease.InExpo);
        
        Destroy(gameObject , ranTimeB+3);
    }
}
