using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YuehLoopBG : MonoBehaviour
{
    public static YuehLoopBG instance;
    public GameObject goBG;
    public GameObject goBG2;
    public GameObject goFG;
    public GameObject goFG2;

    public GameObject all;

    private void Awake() 
    {
        if(instance == null)
        {
            instance = this;
        }
    }
    float startXBG;
    float startXBG2;
    float startXFG;
    float startXFG2;

    private void Start() {
        startXBG = goBG.transform.localPosition.x;
        startXBG2 = goBG2.transform.localPosition.x;
        startXFG = goFG.transform.localPosition.x;
        startXFG2 = goFG2.transform.localPosition.x;
    }

    private void Update() {
        if(isMoving)
        {
            goBG.transform.localPosition -= new Vector3(Time.deltaTime * 5 , 0 , 0);
            goBG2.transform.localPosition -= new Vector3(Time.deltaTime * 5 , 0 , 0);
            goFG.transform.localPosition -= new Vector3(Time.deltaTime * 100 , 0 , 0);
            goFG2.transform.localPosition -= new Vector3(Time.deltaTime * 100 , 0 , 0);

            if(goBG.transform.localPosition.x <= startXBG -1920)
            {
                goBG.transform.localPosition += new Vector3(1920+1920 ,0 ,0);
            }
            if(goBG2.transform.localPosition.x <= startXBG2 - 1920 - 1920)
            {
                goBG2.transform.localPosition += new Vector3(1920+1920 ,0 ,0);
            }

            if(goFG.transform.localPosition.x <= startXFG -1920)
            {
                goFG.transform.localPosition += new Vector3(1920+1920 ,0 ,0);
            }
            if(goFG2.transform.localPosition.x <= startXFG2 - 1920 - 1920)
            {
                goFG2.transform.localPosition += new Vector3(1920+1920 ,0 ,0);
            }
            
        }

    }

    bool isMoving;
    public void DoMove(bool _value = false)
    {
        isMoving = _value;
    }

    public void OpenBG(bool _value)
    {
        all.SetActive(_value);
    }
}
