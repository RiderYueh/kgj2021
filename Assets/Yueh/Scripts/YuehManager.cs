using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YuehManager : MonoBehaviour
{
    static public YuehManager instance;
    private void Awake() {
        if(instance != null) return;
        instance = this;
    }

    private void Start() {
        Application.runInBackground = false;
    }

    private void Update() {
#if UNITY_EDITOR
        if(Input.GetKeyDown("k"))
        {
            BattleManager.instance.ERole.TakeDmg(99999);
        }

        if(Input.GetKeyDown("="))
        {
            Time.timeScale += 1;
        }
        if (Input.GetKeyDown("-"))
        {
            Time.timeScale = 1;
        }
        if(Input.GetKeyDown("l"))
        {
            BattleManager.instance.LearnTwoSkill();
        }
        if(Input.GetKeyDown("m"))
        {
            YuehEvent.instance.OpenEvent();
        }

        if (Input.GetKeyDown("e"))
        {
            BattleManager.instance.EndGame();
        }

        if(Input.GetKeyDown("s"))
        {
            DialogueManager.Instance.PlayLast();
        }
        if (Input.GetKeyDown("1"))
        {
            BattleManager.instance.stageNow += 1;
        }

        if (Input.GetKeyDown("p"))
        {
            BattleManager.instance.PlaySFX(Effect.HitEnemyBreak, false);
            SoundHandler.Instance.PlaySFX(Sound.EnemyBreak);
        }

#endif

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Time.timeScale >= 1)
            {
                Time.timeScale = 0;
                YuehPlayerData.instance.goPause.SetActive(true);
            }
            else
            {
                Time.timeScale = 1;
                YuehPlayerData.instance.goPause.SetActive(false);
            }
        }
    }
}
