using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class YuehMenu : MonoBehaviour
{
    public static YuehMenu instance;
    public GameObject goHead;
    public GameObject goName;
    public Image main_Image;

    public GameObject goBG;
    public GameObject goPlayerData;
    public Text textPlayerName;
    public List<Sprite> list_Sprite;
    public Sprite bonusHeadIcon;

    public Sprite guSprite;
    public Sprite guYouSprite;
    public Sprite tlgSprite;
    public Sprite sgpSprite;

    public Sprite wupoSprite;
    public Sprite TraderSprite;
    public Sprite gobSprite;
    public Sprite guNuSprite;
    public Sprite piGobSprite;

    public Image m_Image;
    public GameObject keyboardParent;
    public GameObject keyboardPrefab;
    public Text textName;

    private int imageNow = 0;

    private void Awake() {
        if(instance == null) instance = this;
    }
    private void Start() {
        m_Image.sprite = list_Sprite[imageNow];
        CreateKeyBoard();
        textName.text = "";
        Cursor.visible = true;

        goHead.SetActive(true);
        goName.SetActive(false);
        goPlayerData.SetActive(false);
    }

    public void OnClickLeft()
    {
        SoundHandler.Instance.PlaySFX(Sound.Button);
        if (imageNow <=0)
        {
            imageNow = list_Sprite.Count - 1;
        }
        else
        {
            imageNow -= 1;
        }
        m_Image.sprite = list_Sprite[imageNow];
    }

    public void OnClickRight()
    {
        SoundHandler.Instance.PlaySFX(Sound.Button);
        if (imageNow >= list_Sprite.Count -1)
        {
            imageNow = 0;
        }
        else
        {
            imageNow += 1;
        }
        m_Image.sprite = list_Sprite[imageNow];
    }

    public void OnClickOK()
    {
        SoundHandler.Instance.PlaySFX(Sound.Button);
        Debug.Log("OK");
        goHead.SetActive(false);
        goName.SetActive(true);
    }

    void CreateKeyBoard()
    {
        string tempName = "王韓宇守物宅平迅閣二十丁太陣國入近收好她士工瑜土寸下廢小口魯勺久凡及夕門女飛刃習叉馬物";
        for(int i = 0; i < tempName.Length ; i++)
        {
            GameObject go = Instantiate(keyboardPrefab , keyboardParent.transform) as GameObject;
            go.SetActive(true);
            Text tempText = go.transform.GetChild(0).GetComponent<Text>();
            tempText.text = tempName.Substring(i,1);
        }
    }

    public void OnClickText(GameObject go)
    {
        SoundHandler.Instance.PlaySFX(Sound.Button);
        textName.text += go.GetComponent<Text>().text;
    }

    public void OnClickStart()
    {
        Debug.Log("Start");
        SoundHandler.Instance.PlaySFX(Sound.Lottery);

        goName.SetActive(false);
        goBG.SetActive(false);
        
        main_Image.sprite = GetPlayerHeadIcon();
        textPlayerName.text = GetPlayerName();

        goPlayerData.SetActive(true);
    }

    public Sprite GetPlayerHeadIcon()
    {

        if (textName.text.Contains("國瑜廢物"))
        {
            return guYouSprite;
        }


        if (textName.text.Contains("國瑜"))
        {
            return guSprite;
        }

        if(textName.text.Contains("魯閣"))
        {
            return tlgSprite;
        }

        if(textName.text.Contains("近平"))
        {
            return sgpSprite;
        }

        

        return m_Image.sprite;
    }

    public string GetPlayerName()
    {
        return textName.text;
    }

    public enum EnumColor
    {
        Blue,
        Green,
        Red,
        White
    }

    public void OnChangeHeadColor(EnumColor ec = EnumColor.Blue)
    {
        if(ec == EnumColor.Blue)
        {
            main_Image.color -= new Color(0.4f , 0.4f ,0);
        }
        if (ec == EnumColor.Red)
        {
            main_Image.color -= new Color(0, 0.4f, 0.4f);
        }
        if (ec == EnumColor.Green)
        {
            main_Image.color -= new Color(0.4f, 0, 0.4f);
        }
        if (ec == EnumColor.White)
        {
            main_Image.color = Color.white;
        }
        main_Image.color = new Color(main_Image.color.r, main_Image.color.g, main_Image.color.b, 1);
        BattleManager.instance.PRoleHead.color = new Color(main_Image.color.r, main_Image.color.g, main_Image.color.b, 1);
    }

    public void OnBounsHead()
    {
        main_Image.sprite = bonusHeadIcon;
        BattleManager.instance.PRoleHead.sprite = bonusHeadIcon;

        main_Image.color = Color.white;
        BattleManager.instance.PRoleHead.color = Color.white;
    }

    public void DoFadeInOut()
    {

    }
}
