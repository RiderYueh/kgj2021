using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YuehPlayerData : MonoBehaviour
{
    public static YuehPlayerData instance;
    public AudioSource m_AudioSource;
    public GameObject goPause;
    public GameObject goYuehGetSkillPrefab;
    public int HP;
    public List<int> List_Wupo; //巫婆藥水

    public int BreakOrNotCount; //營火或戰鬥骰的次數
    public int RanSkillCount;
    public int lockSkillCount;//被商人鎖住

    private void Awake() {
        if(instance == null)
        {
            instance =  this;
        }
    }

    private void Start() {
        HP = 100;
        List_Wupo = new List<int>();
        BreakOrNotCount = 1;
        RanSkillCount = 1;
        lockSkillCount = 0;
    }

    

    public void ShowDialog(string skillName , float ranTimeA = 3f , float ranTimeB = 5f)
    {
        GameObject go = Instantiate(goYuehGetSkillPrefab , transform);
        go.GetComponent<YuehGetSkillPrefab>().Init(skillName , ranTimeA , ranTimeB);
    }

    public void ShowDialogBig(string skillName)
    {
        GameObject go = Instantiate(goYuehGetSkillPrefab, transform);
        go.transform.localScale = new Vector3(1.5f,1.5f,1.5f);
        go.GetComponent<YuehGetSkillPrefab>().Init(skillName, 6,8);
    }

    public int GetBreakOrNotCost()
    {
        if(BreakOrNotCount < 5)
        {
            return 1;
        }
        else if(BreakOrNotCount < 10)
        {
            return 2;
        }

        return 3;
    }

    public int GetRanSkillCost()
    {
        if (RanSkillCount < 5)
        {
            return 1;
        }
        else if (RanSkillCount < 10)
        {
            return 2;
        }
        return 3;
    }
}
